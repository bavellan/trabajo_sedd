# Trabajo SED 2020/2021
![Imagen interior](https://bitbucket.org/bavellan/trabajo_sedd/raw/acea5c8fe4a1152882c6de4c167a20742d343c5b/Imagen.jpg)
## David Andía Pardo 53816
## Billy Avellán Salazar 53828
## Daniel Bajo Collados 53832
### Caja fuerte

El trabajo consiste en una caja fuerte que se abre al introducir correctamente una contraseña de cuatro dígitos.
Para enviarle información al usuario se dispone de una pantalla LCD, y para seleccionar el número se usa un dial con un botón.

El control del proyecto se lleva a cabo mediante una placa FPGA Nexys 4DDR de Xilinx y un microprocesador de arquitectura ARM Cortex-M4. En particular, se usa el microprocesador STM32F407VGT6 de ST Microelectronics, incorporada a la placa STM32F407-DISC1.
La FPGA se usa para controlar la máquina de estados, al igual que determinar si la contraseña introducida es o no correcta.
Por otro lado, la placa STM32 controla los periféricos, consistentes en la pantalla, el dial, el botón y un servomotor que controla la apertura y cierre de la cerradura.

La comunicación entre las placas se lleva a cabo mediante 9 cables, 6 para la comunicación Micro-FPGA y 3 para la comunicación FPGA-Micro.
Con 4 cables, el micro le envía a la FPGA los dígitos de la contraseña en formato binario, usándose el quinto cable como señal del envío de la información.
Con otro le envía que va a comenzar a escribir la contraseña, y con otro que ha escrito un dígito.
Se usa el mismo método para que la FPGA le envíe si ha recibido la señal de que va a comenzar a introducir la contraseña, si ha recibido un dígito y si la contraseña recibida es correcta o incorrecta.

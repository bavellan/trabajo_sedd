----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 31.01.2021 20:48:08
-- Design Name: 
-- Module Name: i2c_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity i2c_tb is
--  Port ( );
end i2c_tb;

architecture Behavioral of i2c_tb is

component i2c_dir
port (  clk: in std_logic;
        --scl: inout std_logic;
        --sda: inout std_logic;
        rst: in std_logic
        );
end component;

signal read_req         : std_logic;
signal data_to_master   : std_logic;--_vector(7 downto 0);
signal data_valid       : std_logic;
signal data_from_master : std_logic_vector(3 downto 0);
signal clk              : std_logic;
signal data_reg : unsigned(data_from_master'range); --std_logic_vector(3 downto 0);--Guarda la entrada del micro7
signal flag: std_logic:='0';
signal counter,counter2: integer range 0 to 3;
signal rst              : std_logic;
TYPE digit is (D0, D1,D2,D3,D4);
    signal dig, next_dig:digit;
begin
uut: i2c_dir port map(
    clk=>clk,
    --sda=>sda,
    --scl=>scl,
    rst=>rst
    );
    
process
    --variables

begin
    --bucle
                                --primera prueba 1 error no reset
rst<='1';
wait for 30 ns;
data_from_master<="0000";
wait for 30 ns;
data_from_master<="0001";
wait for 30 ns;
data_from_master<="0001";
wait for 30 ns;
data_from_master<="0010";
wait for 30 ns;
data_from_master<="0011";
wait for 30 ns;
data_from_master<="0100";
                            --segunda prueba 1 error y reset
wait for 300 ns;
rst<='0';
wait for 30 ns;
data_from_master<="0001";
wait for 30 ns;
data_from_master<="0010";
wait for 30 ns;
data_from_master<="0001";
wait for 30 ns;
data_from_master<="0011";
wait for 30 ns;
data_from_master<="0100";
                            --tercera prueba 1 error y reset
wait for 300 ns;
rst<='1';
wait for 30 ns;
data_from_master<="0001";
wait for 30 ns;
data_from_master<="0010";
wait for 30 ns;
data_from_master<="0001";
wait for 30 ns;
rst<='0';
data_from_master<="0011";
wait for 30 ns;
data_from_master<="0100";
                                --cuarta prueba 3 errores no reset
wait for 300 ns;
rst<='1';
wait for 30 ns;
data_from_master<="0001";
wait for 30 ns;
data_from_master<="0100";
wait for 30 ns;
data_from_master<="0001";
wait for 30 ns;
data_from_master<="0011";
wait for 30 ns;
data_from_master<="0100";
                                --quinta prueba 2 errores no reset
wait for 300 ns;
rst<='1';
wait for 30 ns;
data_from_master<="0001";
wait for 30 ns;
data_from_master<="0100";
wait for 30 ns;
data_from_master<="0010";
wait for 30 ns;
data_from_master<="0011";
wait for 30 ns;
data_from_master<="0110";
wait for 30 ns;
data_from_master<="0100";



                            
end process;

p_clk : process
begin
    clk <='0';
    wait for 20 ns;
    clk <='1';
    wait for 20 ns;
end process;
end Behavioral;
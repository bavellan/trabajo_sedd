----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01.02.2021 12:28:54
-- Design Name: 
-- Module Name: i2c_dir - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity i2c_dir is
    Port ( scl : inout STD_LOGIC;
           sda : inout STD_LOGIC;
           clk : in STD_LOGIC;
           rst : in STD_LOGIC);
end i2c_dir;

architecture Behavioral of i2c_dir is
TYPE digit is (D0, D1,D2,D3,D4);

    signal dig : digit :=D0;
    signal next_dig : digit :=D0;
    
component I2CFuncion
  port (
    scl              : inout std_logic;
    sda              : inout std_logic;
    clk              : in    std_logic;
    rst              : in    std_logic;
    read_req        : out std_logic;
    data_to_master  : in std_logic;
    data_valid      : out std_logic;
    data_from_master: out std_logic_vector(3 downto 0)
    );
end component;


signal read_req         : std_logic;
signal data_to_master   : std_logic;--_vector(7 downto 0);
signal data_valid       : std_logic;
signal data_from_master : std_logic_vector(3 downto 0);

signal data_reg : unsigned(data_from_master'range);-- std_logic_vector(3 downto 0);--Guarda la entrada del micro7
signal flag: std_logic:='0';
--signal counter, counter2: integer range 0 to 3;
shared variable counter, counter2 : integer range 0 to 3;


begin

--  i2c_slave0 : entity work.I2CFuncion(arch)
--  port map(scl,sda,clk,rst, read_req, data_to_master,data_valid,data_from_master);

--  led_o          <= data_reg;
  --data_to_master <= flag;--manda el valor de la fpga al micro
inst_i2cfuncion: I2CFuncion port map(
    scl=>scl,
    sda=>sda,
    clk=>clk,
    rst=>rst,
    
    read_req=>read_req,
    data_to_master=>data_to_master,
    data_valid=>data_valid,
    data_from_master=>data_from_master
);

-- ODDR_inst : ODDR
--   generic map(
--      DDR_CLK_EDGE => "OPPOSITE_EDGE", -- "OPPOSITE_EDGE" or "SAME_EDGE" 
--      INIT => '0',   -- Initial value for Q port ('1' or '0')
--      SRTYPE => "SYNC") -- Reset Type ("ASYNC" or "SYNC")
--   port map (
--      Q => Q,   -- 1-bit DDR output
--      C => C,    -- 1-bit clock input
--      CE => CE,  -- 1-bit clock enable input
--      D1 => D1,  -- 1-bit data input (positive edge)
--      D2 => D2,  -- 1-bit data input (negative edge)
--      R => R,    -- 1-bit reset input
--      S => S     -- 1-bit set input
--   );

  digit_register: process(clk, rst)
  begin
  if rst = '0'  then
        dig <= D0;
        counter:=0;
        counter2:=0;
  end if;
  if(rst='1' and rising_edge(clk)) then
        dig <= next_dig;
        if(data_valid='1') then 
            data_reg <= unsigned(data_from_master);--si se percibe dato se guarda en reg 
            --Transforma el binari del micro en entero
        end if;        
    end if;			 
  end process;
  
  
  nxt_state_decoder: process --maquina de estados
  begin
  if counter<3 then
    next_dig<= dig;--el contador aumenta si el digito percibido es distinto al ejemplo de 
    --digitos de contraseņa---1234---
    case dig is
      when D0 =>
        if data_reg = 1 then
          next_dig <= D1; -- 1---
          else
          counter:=counter+1;
           next_dig <= D0;
        end if;
      when D1 =>
        if data_reg = 2 then    -- 12--
          next_dig <= D2;
          else
          counter:=counter+1;
           next_dig <= D0;
        end if;
      when D2 =>
        if data_reg = 3 then    -- 123-
          next_dig <= D3;
        else                
        counter:=counter+1;
          next_dig <= D0;
        end if;
      when D3 =>
        if data_reg = 4 then    -- 1234-
          next_dig <= D4;
        else             
         counter:=counter+1;   
         next_dig <= D0;
        end if;
      when D4 =>
       data_to_master<='1';--si la secuencia es correcta manda un 1 a la micro 
    end case;
    
    
    else
        if flag<='0'then
            data_to_master<='0';--nos equivocamos 3 veces, secuencia incorrecta y vamos al puk; 
            dig<=D0;--Volvemos al estado inicial   
            flag<='1';-- flag a 1 para que al reiniciar el bucle no velva a mandar 0 a la micro
        end if;
        
   if counter2<3 then     
   next_dig<= dig;--el contador aumenta si el digito percibido es distinto al ejemplo de 
         --digitos de contraseņa---1234--- pero esta vez del puk
   case dig is
     when D0 =>
        if data_reg = 1 then
          next_dig <= D1; -- 1---
          else
          counter2:=counter2+1;
           next_dig <= D0;
        end if;
      when D1 =>
        if data_reg = 2 then    -- 12--
          next_dig <= D2;
          else
          counter2:=counter2+1;
           next_dig <= D0;
        end if;
      when D2 =>
        if data_reg = 3 then    -- 123-
          next_dig <= D3;
        else                
        counter2:=counter2+1;
          next_dig <= D0;
        end if;
      when D3 =>
        if data_reg = 4 then    -- 1234-
          next_dig <= D4;
        else             
         counter2:=counter2+1;   
         next_dig <= D0;
        end if;
       when D4 =>
       data_to_master<='1';--si la secuencia es correcta manda un 1 a la micro 
       end case;
    else
     data_to_master<='0';--puk incorrecta
     next_dig <= D0;
     end if;
   end if;
   
   
   if counter>=3 and counter2>=3 and data_reg = 0 then--reinicia todo para volver a empezar
   --siempre que se reciba 0;
                   --       while data_reg /= 0 loop
                   --       end loop;
   --     wait until data_reg <= 0;
        counter:=0;
        counter2:=0;
        flag<='1';
   end if;
   wait;
  end process;
end architecture;
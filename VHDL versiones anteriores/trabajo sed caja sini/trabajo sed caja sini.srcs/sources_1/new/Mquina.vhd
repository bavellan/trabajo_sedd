library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Mquina is
PORT(
 Comparado: in std_logic_vector(1 downto 0);
 Puerta:    in std_logic;
 Rst:       in std_logic;
 Clk:       in std_logic;
 Estado:   out std_logic_vector(2 downto 0); 
 Aviso:    out std_logic);
end Mquina;

architecture Behavioral of Mquina is
 type Es is (E0, E1, E2, E3); 
 signal Cur_Es, next_Es: Es;
 signal Reg_Comp:std_logic_vector(1 downto 0);
 signal Reg_Pu: std_logic;
 shared variable contador: integer range 0 to 3:=0;
begin
state_reg: process (Clk)
  begin
  Reg_Comp<=Comparado;
    if rising_edge(Clk) then
      if Rst = '0' then
       Cur_Es <= E0;
      else
        Cur_Es <= next_Es;
      end if;        
    end if;
  end process;
  next_state_decoder: process (Cur_Es, Comparado)
  begin
    next_Es <= Cur_Es;
    case Cur_Es is
      when E0 =>
      Estado<="000";
      Aviso<='1';
      if rising_edge(Comparado(1)) then
        if Comparado(0)='0' then 
          next_Es<=E1;
          Aviso<='1';
        else
          Contador:=Contador+1;
           Estado<="000";
          if Contador=3 then
            Contador:=0;
            next_Es<=E2;
            Aviso<='1';
          end if;
        end if;
        else     Aviso<='0';
     end if;
        
      when E1 =>
       Estado<="001";
      if Puerta='1' then
       next_Es<=E0;
       Aviso<='1';
       else
       Aviso<='0';
      end if;
      
      when E2 =>
       Estado<="010";
       Aviso<='1';
      if rising_edge(Comparado(1)) then
        if Comparado(0)='0' then 
          next_Es<=E1;
          Aviso<='1';
        else
          Contador:=Contador+1;
          Estado<="010";
          if Contador=3 then
            Contador:=0;
            next_Es<=E3;
            Aviso<='1';
          end if;
        end if;
         else     Aviso<='0';
     end if;
     
      when E3 =>
      Estado<="011";
      if Rst<='0' then 
        next_Es<=E0;
        Aviso<='1';
      else
        Aviso<='0';
      end if;
      end case;
    end process;
       
     end Behavioral;

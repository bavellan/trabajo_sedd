----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01.02.2021 22:01:37
-- Design Name: 
-- Module Name: top_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top_tb is
    
end top_tb;

architecture Behavioral of top_tb is
            --componentes, se�ales
component top 
Port ( digitos : in STD_LOGIC_VECTOR (3 downto 0);   --digito entrada stm-comp
       --S0 : out STD_LOGIC;                  --
       clk : in STD_LOGIC;                  --va ambas
       rst : in STD_LOGIC;                  --va maquina
       puerta : in std_logic;                           --stm-maquina
       pushbutton : in std_logic;                  --stm-comp
       estado : out std_logic_vector(2 downto 0);       --maquina-stm         
       aviso : out std_logic                            --maquina-stm
       );                       --entradas 8    salidas 4
end component;
signal digitos : std_logic_vector(3 downto 0);
--signal S0 : std_logic;
signal clk : std_logic;
signal rst : std_logic;
signal puerta : std_logic;
signal pushbutton : std_logic;
signal estado : std_logic_vector(2 downto 0);
signal aviso : std_logic;
--signal comparado : std_logic_vector (1 downto 0);

--signal counter, counter2 : integer range 0 to 3;
begin
            --uut, process
uut: top port map(
    digitos=>digitos,
  --  S0=>S0,
    clk=>clk,
    rst=>rst,
    puerta=>puerta,
    pushbutton=>pushbutton,
    estado => estado,
    aviso=>aviso
    );



p_clk : process
begin
    clk <='0';
    wait for 2 us;
    clk <='1';
    wait for 2 us;
end process;


digit: process
begin
 --  wait for 1 us;             --sin fallo
    digitos<="0001";            
    wait for 6 us;             
    digitos<="0010";
    wait for 6 us;             
    digitos<="0011";
    wait for 6 us;             
    digitos<="0100";
    wait for 6 us;             
    digitos<="1000";
    wait for 100 us;            --separar
    
    
    digitos<="0000";        --fallo inicial
    wait for 1 us;             
    digitos<="1001";
    wait for 6 us;             
    digitos<="0101";
    wait for 6 us;             
    digitos<="0011";
    wait for 6 us;             
    digitos<="0100";                --lobby 210
 --   wait for 60 ns;             
 --   digitos<="1000";
    
    wait for 6 us;            --fallo mitad
    digitos<="0001";
    wait for 6 us;             
    digitos<="0010";
    wait for 6 us;             
    digitos<="0010";
    wait for 6 us;             
    digitos<="0110";
    wait for 6 us;             
    digitos<="1000";
    wait for 6 us;                 --pal lobby 570
    
    
    
    wait for 6 us;             --fallo final
    digitos<="0001";            
    wait for 6 us;             
    digitos<="0010";
    wait for 6 us;             
    digitos<="0011";
    wait for 6 us;             
    digitos<="0110";
    wait for 6 us;             
    digitos<="1000";
    wait for 6 us;             
    digitos<="0110";
    wait for 6 us;                 --pal lobby, pero no se ha ido, deberia estar en el lobby de 1000 a 1020
    digitos<="1000";                --otra opcion es magia negra en el tercer fallo y el lobby coincide con el flanco negativo del clk y del tercer intento
    wait for 6 us;                 --tampoco deberia estar este wait
    
    
    --  wait for 1 us;             --sin fallo
    digitos<="0001";            
    wait for 6 us;             
    digitos<="0010";
    wait for 6 us;             
    digitos<="0011";
    wait for 6 us;             
    digitos<="0100";
    wait for 6 us;             
    digitos<="1000";
    wait for 100 us;            --separar
 
    
end process;

puerta_proc: process
begin
    puerta <='0';
    wait for 233 us;
    puerta <='1';
    wait for 233 us;
end process;


rst_proc:process
begin
    rst<='1';
    wait for 466 us;
    rst<='0';
    wait for 466 us;
end process;


sig: process
begin
--wait for 6 us;
    pushbutton<='0';
    wait for 3 us;
    pushbutton<='1';
    wait for 3 us;
end process;






end Behavioral;

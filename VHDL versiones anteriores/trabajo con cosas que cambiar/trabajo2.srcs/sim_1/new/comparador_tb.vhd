----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.02.2021 02:13:39
-- Design Name: 
-- Module Name: comparador_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity comparador_tb is
--  Port ( );
end comparador_tb;

architecture Behavioral of comparador_tb is

component comparador
port (digitos : in STD_LOGIC_VECTOR (3 downto 0);
      comparacion : inout STD_LOGIC_VECTOR (1 downto 0);
      clk : in STD_LOGIC;
 --     rst : in STD_LOGIC;
      sigdig: in std_logic;
      aux : out std_logic_vector(3 downto 0)
);
end component;
signal digitos : std_logic_vector (3 downto 0);
signal comparacion : std_logic_vector (1 downto 0):="00";
signal clk : std_logic;
signal sigdig : std_logic;
signal aux : std_logic_vector(3 downto 0);

begin
uut: comparador port map(
    digitos => digitos,
    comparacion => comparacion,
    clk => clk,
    sigdig => sigdig,
    aux => aux
);

digit: process
begin
   digitos<="0000";        --fallo inicial
    wait for 2 us;             
    digitos<="0001";
    wait for 6 us;             
    digitos<="0101";
    wait for 6 us;             
    digitos<="0011";
    wait for 6 us;             
    digitos<="0100";                --lobby 19 -> 22            184
 --   wait for 60 ns;             
 --   digitos<="1000";
    
    wait for 6 us;            --fallo mitad
    digitos<="0001";
    wait for 6 us;             
    digitos<="0010";
    wait for 6 us;             
    digitos<="0010";
    wait for 6 us;             
    digitos<="0110";
    wait for 6 us;             
    digitos<="1000";
    wait for 6 us;                 --pal lobby 55 -> 58         220
    
    
    
    wait for 6 us;             --fallo final
    digitos<="0001";            
    wait for 6 us;             
    digitos<="0010";
    wait for 6 us;             
    digitos<="0011";                    
    wait for 6 us;             
    digitos<="0110";
    wait for 6 us;             
    digitos<="1000";
    wait for 6 us;             
    digitos<="0110";                                --97 pero xd        26 still xd
    wait for 6 us;                 --pal lobby, pero no se ha ido, deberia estar en el lobby de 1000 a 1020
    digitos<="1000";                --otra opcion es magia negra en el tercer fallo y el lobby coincide con el flanco negativo del clk y del tercer intento
    wait for 6 us;                 --tampoco deberia estar este wait
                    
    wait for 6 us;             --sin fallo
    digitos<="0001";            
    wait for 6 us;             
    digitos<="0010";
    wait for 6 us;             
    digitos<="0011";
    wait for 6 us;             
    digitos<="0100";
    wait for 6 us;             
    digitos<="1000";                --133 -> 135            297
    wait for 30 us;            --separar

end process;

sig: process
begin
    sigdig<='0';
    wait for 3 us;
    sigdig<='1';
    wait for 3 us;
end process;


p_clk : process
begin
    clk <='0';
    wait for 2 us;
    clk <='1';
    wait for 2 us;
end process;

end Behavioral;






















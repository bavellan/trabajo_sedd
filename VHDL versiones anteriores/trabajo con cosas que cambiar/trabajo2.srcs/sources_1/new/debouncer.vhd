----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04.02.2021 20:44:04
-- Design Name: 
-- Module Name: debouncer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity debouncer is
  Port ( 
    CLK      : in std_logic;
    ENTRADA  : in std_logic;
    SALIDA   : out std_logic
    );
end debouncer;

architecture Behavioral of debouncer is
signal reg : std_logic_vector(2 downto 0);
begin
process (CLK)
begin
    if rising_edge(CLK) then
        reg(2) <= reg(1);
        reg(1) <= reg(0);
        reg(0) <= ENTRADA;
        if reg = "111" then
            SALIDA <= '1';
        else 
            SALIDA <= '0';
        end if;
    end if;
 end process;

end Behavioral;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity comparador is
generic (D0 : STD_LOGIC_VECTOR (3 downto 0) :="0001";
         D1 : STD_LOGIC_VECTOR (3 downto 0) :="0010";
         D2 : STD_LOGIC_VECTOR (3 downto 0) :="0011";
         D3 : STD_LOGIC_VECTOR (3 downto 0) :="0100";
         Daux : std_logic_vector(3 downto 0) := "1111" );
Port (   digitos : in STD_LOGIC_VECTOR (3 downto 0):="0001";        --viene de stm
         comparacion : inout STD_LOGIC_VECTOR (1 downto 0):="00";       --comparacion con contraseņa guardada
         clk : in STD_LOGIC;                                    --va
         sigdig: in std_logic;                               --viene de stm
         aux : out std_logic_vector(3 downto 0) :="0000"     --no se para que era

         );         --entrada 6   salida 2
end comparador;

architecture Behavioral of comparador is

signal pass_fail : std_logic :='0';
signal tres_fallos : std_logic :='0';
signal Dauxs : std_logic_vector(3 downto 0) :="0000";   --ir cambiando de digito
signal Dauxs2 : std_logic_vector(3 downto 0) :="0000";

begin

comp: process(clk,sigdig,digitos)
begin

    comparacion <= "00";
        if rising_edge(sigdig) then
            if digitos = D0 or Dauxs(0)=Daux(0) then
                Dauxs(0) <='1';                                     --digito 1
                    if digitos = D1 or Dauxs(1)=Daux(1) then
                        Dauxs(1)<='1';                              --digito 2
                            if digitos = D2 or Dauxs(2)=Daux(2) then
                                Dauxs(2)<='1';                      --digito 3
                                    if digitos = D3 or Dauxs(3)=Daux(3) then
                                        Dauxs(3)<='1';              --digito 4
                                    else
                                        aux<="1111";        --fallo 4
                                    end if;
                            else
                                aux<="1110";                --fallo 3
                            end if;
                    else
                        aux<="1101";                        --fallo 2
                    end if;
            else
                aux<="1100";                                --fallo 1
            end if;
                --aqui prro
            case Dauxs is
                when "0000" =>
                        comparacion<="01";
                when "0001" =>                                  --digito 1
                    if Dauxs2(0) ='1' then
                        comparacion <= "11";
                        aux <= "1010";
                    else
                        aux <="0001";
                        Dauxs2(0) <='1';
                    end if;
                when "0011" =>                                  --digito 2
                    if Dauxs2(1)='1' then
                        comparacion <= "11";
                        aux <= "1010";
                    else
                        aux <="0010";
                        Dauxs2(1)<='1';
                    end if;
                when "0111"=>                                  --digito 3
                    if Dauxs2(2)='1' then
                        comparacion <= "11";
                        aux <= "1010";
                    else
                        aux <="0011";
                        Dauxs2(2)<='1';
                    end if;
                when "1111"=>                                  --digito 4
                    if Dauxs2(3)='1' then
                        comparacion <= "11";
                    else 
                        aux<="0100";
                        Dauxs2(3)<='1';
                        comparacion <= "10";
                    end if;
                when others =>
                    comparacion <= "11";
            end case;
        end if;
    if comparacion="11" or comparacion ="10" then       --reiniciar los intentos
        Dauxs <= "0000";
        Dauxs2 <= "0000";
        aux <= "0000";
    end if;
    
end process;


end Behavioral;
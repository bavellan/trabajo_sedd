-- Code your design here
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;


entity new_clk is
port (
   clk :in std_logic;
   clk2 :out std_logic
);
end new_clk;

architecture behavioral of new_clk is

signal counter : integer range 0 to 500000 :=0;
signal clk_interno :std_logic:='0';
begin

gestion_CLK: process(clk,counter)

begin

    if rising_edge(clk) then
        counter <= counter + 1;
        if counter >= 100000 then       ----- VALOR REAL 50000 , VALOR TESTBENCH 10
        --if counter >= 10 then         ----- valor testbench
            clk_interno <= not(clk_interno) ;
            counter <= 0;
        end if;
    end if;

end process;
clk2 <= clk_interno;
end behavioral;
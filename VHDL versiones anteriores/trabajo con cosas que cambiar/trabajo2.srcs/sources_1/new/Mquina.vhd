library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Mquina is
PORT(
     Comparado: in std_logic_vector(1 downto 0);    --comparacion de fpga
     Puerta:    in std_logic;       -- de la stm
     Rst:       in std_logic;       --va
     Clk:       in std_logic;       --va
     Estado:   out std_logic_vector(2 downto 0):="000";     --a la stm
     Aviso:    out std_logic:='0');                              --a la stm
end Mquina;         --entrada 5    salida 4

architecture Behavioral of Mquina is
 type Es is (E0, E1, E2, E3); 
 signal Cur_Es, next_Es: Es :=E0;
 shared variable i : std_logic := '0';
 
 --signal Reg_Comp:std_logic_vector(1 downto 0);
 --signal Reg_Pu: std_logic;
-- shared variable contador: integer range 0 to 3:=0;

begin
state_reg: process (Clk)
begin
    if rising_edge(Clk) then
    
        if Rst = '0' then
            Cur_Es <= E0;  
            --aviso<='1'; 
        else
            Cur_Es <= next_Es;
        end if;
     else
 
    end if;
end process;
next_state_decoder: process (Cur_Es, Comparado,Puerta)                --frecuencia, debouncer, mas estados??
begin
   -- next_Es <= Cur_Es;
    case Cur_Es is
        when E0 =>                                --E0
            if (Comparado(1)='1') then
                if Comparado(0)='0' then 
                    next_Es<=E1;--abrir
                else
                    next_Es<=E2;--error
                end if;
            else
                if comparado="01" then
                    if i='1' then
                        next_Es <=E2;
                    else
                        i:='1';
                    end if;
                end if;
            end if;
       
        when E1 =>                                --E1 abrir
            if Puerta='1' then
                  next_Es<=E0;
            end if;
          
        when E2 =>                                --E2 error
            if (Comparado(1)='1') then
                if Comparado(0)='0' then 
                    next_Es<=E1;
                else
                    next_Es<=E3;
                end if;
            end if;
               
        when E3 =>                              --E3
            if Rst<='0' then 
                  next_Es<=E0;
            end if;
        when others =>
            next_Es <=E0;
    end case;
end process;
       
       
salida_decod : process(cur_es)
begin
case cur_es is
    when E0 =>
        Estado<="000";
    when E1 =>
        Estado<="001";
    when E2 =>
        Estado<="010";
    when E3 =>
        Estado<="011";
    when others =>
        Estado<="000";
        
end case;
end process;
    
otro: process(cur_es, next_es)

begin

    if next_es=cur_es then
        aviso<='0';
    else
        aviso<='1';
    end if;


end process;

end Behavioral;





library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity top is
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           puerta : in STD_LOGIC;
           pushbutton : in std_logic;
           --comparado : in std_logic_vector (1 downto 0);
           digitos : in STD_LOGIC_VECTOR (3 downto 0);
           estado : out STD_LOGIC_VECTOR (2 downto 0):="000";
           aviso : out STD_LOGIC
         --  aux : out std_logic_vector(3 downto 0)
);
end top;


architecture Behavioral of top is
--componentes, se�ales, estados

component comparador 
port ( digitos : in STD_LOGIC_VECTOR (3 downto 0);
       comparacion : inout STD_LOGIC_VECTOR (1 downto 0);
       clk : in STD_LOGIC;
       --rst : in STD_LOGIC;
       sigdig : in std_logic
       --aux : out std_logic_vector(3 downto 0)
);
end component;

component mquina
port (
    comparado : in STD_LOGIC_VECTOR (1 downto 0);
    puerta : in STD_LOGIC;
    rst : in STD_LOGIC;
    clk : in STD_LOGIC;
    estado : out STD_LOGIC_VECTOR (2 downto 0);
    aviso : out STD_LOGIC
);
end component;


signal comparado : std_logic_vector (1 downto 0);
--signal comparacion : std_logic_vector(1 downto 0);
signal aux: std_logic_vector(3 downto 0);


begin
--instanciaciones, process

inst_comparador: comparador port map(
    digitos => digitos,
    comparacion => comparado,
    clk => clk,
    --rst=> rst,
    sigdig => pushbutton
    --aux => aux
);

inst_mquina: mquina port map(
    comparado => comparado,
    puerta => puerta,
    rst => rst,
    clk => clk,
    estado => estado,
    aviso => aviso
);


end Behavioral;

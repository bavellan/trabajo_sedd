----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.02.2021 00:28:59
-- Design Name: 
-- Module Name: comparador - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity comparador is
generic (D0 : STD_LOGIC_VECTOR (3 downto 0) :="0001";
         D1 : STD_LOGIC_VECTOR (3 downto 0) :="0010";
         D2 : STD_LOGIC_VECTOR (3 downto 0) :="0011";
         D3 : STD_LOGIC_VECTOR (3 downto 0) :="0100");
Port (   digitos : in STD_LOGIC_VECTOR (3 downto 0);
         comparacion : out STD_LOGIC_VECTOR (1 downto 0);
         clk : in STD_LOGIC;
         sigdig: in std_logic;
         aux : out std_logic_vector(3 downto 0) :="0000"
         );
end comparador;

architecture Behavioral of comparador is

signal pass_fail : std_logic :='0';
signal tres_fallos : std_logic :='0';
shared variable cuenta_intentos : integer range 0 to 2 :=0;
shared variable cuenta_numeros : integer range 0 to 3 :=0;

begin
comp: process(clk,sigdig,digitos)
begin
    if(rising_edge(clk)) then
        if cuenta_intentos <2 then --empieza a leer la contraseņa para comprobar si esta bien
            if rising_edge(sigdig) then
                if digitos = D0 then
                    cuenta_numeros := cuenta_numeros +1;
                    aux<="0001";
                    if digitos = D1 then
                        cuenta_numeros := cuenta_numeros +1;
                        aux<="0010";
                        if digitos = D2 then
                            cuenta_numeros := cuenta_numeros +1;
                            aux<="0011";
                            if digitos = D3 then
                                cuenta_numeros := cuenta_numeros +1;
                                aux<="0100";
                                comparacion(1)<='1';
                                comparacion(0)<='0';
                            else
                                aux<="1111";
                                cuenta_intentos := cuenta_intentos +1;
                            end if;
                        else
                            aux<="1110";
                            cuenta_intentos := cuenta_intentos +1;
                        end if;
                    else
                        aux<="1101";
                        cuenta_intentos := cuenta_intentos +1;
                    end if;
                else
                    aux<="1100";
                    cuenta_intentos := cuenta_intentos +1;
            end if;
        end if;
        else --ya han pasado los tres intentos y se avisa a la maquina de estados
            aux<="1011";
            cuenta_intentos :=0;
            comparacion(1)<='1';
            comparacion(0)<='1';
        end if;
    end if;
    
end process;












end Behavioral;













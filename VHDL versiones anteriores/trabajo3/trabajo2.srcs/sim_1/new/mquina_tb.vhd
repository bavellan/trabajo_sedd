library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Mquina_tb is

end Mquina_tb;

architecture Behavioral of Mquina_tb is
  -- Component Declaration for the Unit Under Test (UUT)
  component seqdet
    port(
 Comparado: in std_logic_vector(1 downto 0);
 Puerta:    in std_logic;
 Rst:       in std_logic;
 Clk:       in std_logic;
 Estado:   out std_logic_vector(2 downto 0); 
 Aviso:    out std_logic
    );
  end component;

  --Inputs
 signal Comparado:  std_logic_vector(1 downto 0);
 signal Puerta:     std_logic;
 signal Rst:        std_logic;
 signal Clk:        std_logic;

  --Outputs
 signal  Estado:   std_logic_vector(2 downto 0); 
 signal  Aviso:     std_logic;

  -- Clock period definitions
  constant clk_n_period: time := 20 ns;

begin
  -- Instantiate the Unit Under Test (UUT)
  uut: seqdet
    port map (
      Rst => Rst,
      Clk => Clk,
      Puerta=>Puerta,
      Comparado  => Comparado,
      Estado=>Estado,
      Aviso=>Aviso);

  -- Clock process definitions
  clk_n_process :process
  begin
    Clk <= '0';
    wait for 0.5 * clk_n_period;
    Clk <= '1';
    wait for 0.5 * clk_n_period;
  end process;

  -- Stimulus process
  stim_proc: process
  begin
    Comparado(0)<='0';
    Comparado(1)<='0';
    wait for 40 ns;
    Puerta <= '0';
    Comparado(0)<='0';
    Comparado(1)<='1';
    Rst <= '1';
    wait for 40 ns;
    Puerta <= '0';
   -- Comparado(0)<='0';
    --Comparado(1)<='1';
    --Rst <= '1';
    wait for 40 ns;
    Puerta <= '0';
    Comparado(0)<='1';
    Comparado(1)<='1';
    Rst <= '1';
    wait for 40 ns;
    Rst <= '0';

    wait for 3 * clk_n_period;

    assert false
      report "[SUCCESS]: simulation finished."
      severity failure;
  end process;

end Behavioral;



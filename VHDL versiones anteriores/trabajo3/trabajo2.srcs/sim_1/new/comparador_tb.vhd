----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.02.2021 02:13:39
-- Design Name: 
-- Module Name: comparador_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity comparador_tb is
--  Port ( );
end comparador_tb;

architecture Behavioral of comparador_tb is

component comparador
port (digitos : in STD_LOGIC_VECTOR (3 downto 0);
      comparacion : out STD_LOGIC_VECTOR (1 downto 0);
      clk : in STD_LOGIC;
      sigdig: in std_logic;
      aux : out std_logic_vector(3 downto 0)
);
end component;
signal digitos : std_logic_vector (3 downto 0);
signal comparacion : std_logic_vector (1 downto 0):="00";
signal clk : std_logic;
signal sigdig : std_logic;
signal aux : std_logic_vector(3 downto 0);

begin
uut: comparador port map(
    digitos => digitos,
    comparacion => comparacion,
    clk => clk,
    sigdig => sigdig,
    aux => aux
);

tb: process
begin
    sigdig <='0';           --0
    digitos <="0001";       --valor 1
    wait for 30 ns;                         --30ns  470ns
    sigdig<='1';            --fp se carga 1
    wait for 30 ns;                         --60ns  500ns
    digitos <="0010";       --valor 2
    sigdig<='0';            --fn
    wait for 30 ns;                         --90ns  530ns
    sigdig<='1';            --fp se carga 2
    wait for 30 ns;                         --120ns 560ns
    sigdig<='0';            --fn
    wait for 30 ns;                         --150ns 590ns
    digitos <="0011";       -- valor 3
    wait for 30 ns;                         --180ns 620ns
    sigdig <='1';           --fp se carga 3
    wait for 30 ns;                         --210ns 650ns
    sigdig<='0';            --fn
    digitos <="0100";       --valor 4
    wait for 30 ns;                         --240ns 680ns
    sigdig<='1';            --fp se carga 4
    wait for 30 ns;                         --270ns 710ns
    sigdig<='0';
    wait for 30 ns;                         --300ns 740ns
    sigdig <='1';
    wait for 140 ns;                        --440ns 770ns
end process;


p_clk : process
begin
    clk <='0';
    wait for 20 ns;
    clk <='1';
    wait for 20 ns;
end process;

end Behavioral;











----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01.02.2021 22:01:37
-- Design Name: 
-- Module Name: top_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top_tb is
    
end top_tb;

architecture Behavioral of top_tb is
            --componentes, se�ales
component top 
Port ( E : in STD_LOGIC_VECTOR (3 downto 0);
       S0 : out STD_LOGIC;
       clk : in STD_LOGIC;
       rst : in STD_LOGIC);
end component;
signal E : std_logic_vector(3 downto 0);
signal S0 : std_logic;
signal clk : std_logic;
signal rst : std_logic;
--signal counter, counter2 : integer range 0 to 3;
begin
            --uut, process
uut: top port map(
    E=>E,
    S0=>S0,
    clk=>clk,
    rst=>rst

    );

process
--variables
begin
--bucle
rst <='1';                      --3 errores no reset        --1234
E <="0000";
wait for 50 ns;
E <="0001";
wait for 50 ns;
E <="0010";
wait for 50 ns;
E <="0011";
wait for 50 ns;
E <="0100";
wait for 50 ns;                 







end process;

p_clk : process
begin
    clk <='0';
    wait for 20 ns;
    clk <='1';
    wait for 20 ns;
end process;

end Behavioral;

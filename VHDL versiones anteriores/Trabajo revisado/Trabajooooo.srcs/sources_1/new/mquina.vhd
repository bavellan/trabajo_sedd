library ieee;
use ieee.std_logic_1164.all;

package aux_types is
    type code_vector is array (positive range <>) of std_logic_vector(3 downto 0);
end package;

package body aux_types is
end package body;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.aux_types.all;

entity MQUINA is
    generic (
        CODE    : code_vector;
        CODEpuk : code_vector
     );
    port(
        RST_N : in  std_logic;
        CLK   : in  std_logic;
        START : in  std_logic;  -- Request start to send code (from STM)
        DRDY  : in  std_logic;  -- Digit ready to read (from STM)
        DIGIT : in  std_logic_vector(3 downto 0);  -- Digit (from STM)
        DACK  : out std_logic;  -- Digit ready acknowledge (to STM)
        DONE  : out std_logic;  -- Code checking finished (to STM)
        MATCH : out std_logic;   -- Code match (to STM)
        DACKrep  : out std_logic;  -- Digit ready acknowledge (to STM)
        DONErep  : out std_logic;  -- Code checking finished (to STM)
        MATCHrep : out std_logic   -- Code match (to STM)
     );                              
end MQUINA;

architecture BEHAVIORAL of MQUINA is
    type MQUINA_STATE is (
        E0_NOMATCH,   -- Initial state / Invalid code
        E1_STARTACK,  -- Acknowledge start request
        E2_WTDIGIT,   -- Wait for digit
        E3_DIGACK,    -- Acknowledge digit
        E4_MATCH      -- Valid code
    );
    signal state, nxt_state : MQUINA_STATE;
    signal index, nxt_index : positive range CODE'range;
    signal counter: integer :=0;
begin

    state_reg: process (RST_N, CLK)
    begin
        if RST_N = '0' then
            state <= E0_NOMATCH;
            index <= CODE'low;
        elsif rising_edge(clk) then
            state <= nxt_state;
            index <= nxt_index;
        end if;
    end process;

    nxtstate_decoder: process (state, START, DRDY)
    begin
        nxt_state <= state;
        nxt_index <= index;

        case state is
            when E0_NOMATCH =>
                if START = '1' and ( counter = 0 or counter = 3 )then
                    nxt_state <= E1_STARTACK;
                elsif counter > 0 and counter < 3 then  -- Reintroducir la contraseņa
                    nxt_index <= CODE'low;  -- Restart code digit index
                    nxt_state <= E2_WTDIGIT;
                end if;

            when E1_STARTACK =>
                nxt_index <= CODE'low;  -- Restart code digit index
                counter <= 0;           -- poner a 0 el contador de errores
                if START = '0' then
                    nxt_state <= E2_WTDIGIT;
                end if;

            when E2_WTDIGIT =>
                if START = '1' then
                    nxt_state <= E1_STARTACK;
                elsif DRDY = '1' then
                    nxt_state <= E3_DIGACK;
                end if;

            when E3_DIGACK =>
                if START = '1' then
                    nxt_state <= E1_STARTACK;
                elsif counter < 3 then 
                    if DIGIT /= CODE(index) then    -- Comparar con el pin
                    nxt_state <= E0_NOMATCH;
                    counter <= counter + 1;         -- Aumentar el contador de errores
                    elsif DRDY = '0' then
                        if index /= CODE'high then
                            nxt_index <= index + 1;
                            nxt_state <= E2_WTDIGIT;
                        else
                            nxt_state <= E4_MATCH;
                        end if;
                    end if;
                else
                    if DIGIT /= CODEpuk(index) then -- Comparar con el puk
                    nxt_state <= E0_NOMATCH;
                    
                    elsif DRDY = '0' then
                        if index /= CODEpuk'high then
                            nxt_index <= index + 1;
                            nxt_state <= E2_WTDIGIT;
                        else
                            nxt_state <= E4_MATCH;
                        end if;
                    end if;
                end if;

            when E4_MATCH =>
                if START = '1' then
                    nxt_state <= E1_STARTACK;
                end if;
                

            when others =>
                nxt_state <= E0_NOMATCH;
                nxt_index <= CODE'low;
        end case;
    end process;

    output_decoder: process (state)
    begin
        case state is
            when E0_NOMATCH =>
                DACK  <= '0';
                DONE  <= '1';
                MATCH <= '0';
                DACKrep  <= '0';
                DONErep  <= '1';
                MATCHrep <= '0';

            when E1_STARTACK =>
                DACK  <= '1';
                DONE  <= '0';
                MATCH <= '0';
                DACKrep  <= '1';
                DONErep  <= '0';
                MATCHrep <= '0';

            when E2_WTDIGIT =>
                DACK  <= '0';
                DONE  <= '0';
                MATCH <= '0';
                DACKrep  <= '0';
                DONErep  <= '0';
                MATCHrep <= '0';

            when E3_DIGACK =>
                DACK  <= '1';
                DONE  <= '0';
                MATCH <= '0';
                DACKrep  <= '1';
                DONErep  <= '0';
                MATCHrep <= '0';

            when E4_MATCH =>
                DACK  <= '0';
                DONE  <= '1';
                MATCH <= '1';
                DACKrep  <= '0';
                DONErep  <= '1';
                MATCHrep <= '1';

            when others =>
                DACK  <= '0';
                DONE  <= '1';
                MATCH <= '0';
                DACKrep  <= '0';
                DONErep  <= '1';
                MATCHrep <= '0';
        end case;
    end process;
end BEHAVIORAL;

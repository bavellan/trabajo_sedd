----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04.02.2021 20:44:04
-- Design Name: 
-- Module Name: debouncer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DEBOUNCER is
    generic (
        WIDTH    : positive
    );
    port (
        RST_N    : in  std_logic;
        CLK      : in  std_logic;
        ENTRADA  : in  std_logic_vector(WIDTH - 1 downto 0);
        SALIDA   : out std_logic_vector(WIDTH - 1 downto 0)
    );
end DEBOUNCER;

architecture BEHAVIORAL of DEBOUNCER is
    signal salida_i : std_logic_vector(WIDTH - 1 downto 0);
begin
    debouncers: for i in ENTRADA'range generate
    begin
        process (CLK)
            variable reg : std_logic_vector(2 downto 0);
        begin
            if RST_N = '0' then
                reg := (others => '0');
                salida_i(i) <= '0';
            elsif rising_edge(CLK) then
                reg := reg(1 downto 0) & ENTRADA(i);
                if salida_i(i) = '0' then
                    if reg = "111" then
                        salida_i(i) <= '1';
                    end if;
                elsif reg = "000" then
                    salida_i(i) <= '0';
                end if;             
            end if;
        end process;
    end generate;
    SALIDA <= salida_i;
end BEHAVIORAL;

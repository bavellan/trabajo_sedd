library ieee;
use ieee.std_logic_1164.all;

use work.aux_types.all;

entity MQUINA_TB is
end MQUINA_TB;

architecture TB of MQUINA_TB is

    component MQUINA
        generic (
            CODE : code_vector
         );
        port(
            RST_N : in  std_logic;
            CLK   : in  std_logic;
            START : in  std_logic;  -- Start password checking request (from STM)
            DRDY  : in  std_logic;  -- Digit ready (from STM)
            DIGIT : in  std_logic_vector(3 downto 0);  -- Digit (from STM)
            DACK  : out std_logic;  -- Digit acknowledge (to STM)
            DONE  : out std_logic;  -- Password checking finished (to STM)
            MATCH : out std_logic   -- Password match (to STM)
         );                              
    end component;

    -- Inputs
    signal rst_n : std_logic;
    signal clk   : std_logic;
    signal start : std_logic;
    signal drdy  : std_logic;
    signal digit : std_logic_vector(3 downto 0);

    -- Outputs
    signal dack  : std_logic;
    signal done  : std_logic;
    signal match : std_logic;

    constant CLK_PERIOD : time := 10 ns;
    constant GOOD_CODE  : code_vector := ("0001", "0010", "0011", "0100");
    constant BAD_CODE   : code_vector := ("0001", "0010", "0100");
begin
    dut: MQUINA
        generic map (
            CODE => GOOD_CODE
         )
        port map (
            RST_N => rst_n,
            CLK   => clk,
            START => start,
            DRDY  => drdy,
            DIGIT => digit,
            DACK  => dack,
            DONE  => done,
            MATCH => match
        );

    clk_n_process: process
    begin
        clk <= '0';
        wait for 0.5 * CLK_PERIOD;
        clk <= '1';
        wait for 0.5 * CLK_PERIOD;
    end process;

    stimuli: process
    begin
        -- Reset
        wait for 0.25 * CLK_PERIOD;
        rst_n <= '0';
        start <= '0';
        drdy  <= '0';
        digit <= "0000";
        wait for 0.5 * CLK_PERIOD;
        rst_n <='1';
        wait until clk = '1';

        -- Enter good code ----------------------------------------------------
        -- Request start sending code
        start <= '1';

        -- Wait start acknowledge
        wait until dack = '1' for 2 * CLK_PERIOD;
        assert dack = '1'
            report "[FAILURE]: acknowledge failed."
            severity failure;

        -- Wait start acknowledge deassertion
        start <= '0';
        wait until dack = '0' for 2 * CLK_PERIOD;
        assert dack = '0'
            report "[FAILURE]: deassert start acknowledge failed."
            severity failure;

        -- Send code digit by digit
        for i in GOOD_CODE'range loop
            -- Send digit
            digit <= GOOD_CODE(i);
            drdy  <= '1';
 
            -- Wait digit acknowledge
            wait until dack = '1' for 2 * CLK_PERIOD;
            assert dack = '1'
                report "[FAILURE]: digit acknowledge failed."
                severity failure;
 
            -- Wait digit acknowledge deassertion
            drdy <= '0';
            wait until dack = '0' for 2 * CLK_PERIOD;
            assert dack = '0'
                report "[FAILURE]: deassert digit acknowledge failed."
                severity failure;
        end loop;

        -- Check DONE signal asserted
        wait until done = '1' for 2 * CLK_PERIOD;
        assert done = '1'
            report "[FAILURE]: comparision should be finished by now."
            severity failure;
        -- Check good code signaled.
        assert match = '1'
            report "[FAILURE]: comparision failed."
            severity failure;            
        ---------------------------------------------------- Enter good code --

        for i in 1 to 2 loop
            wait until clk = '1';
        end loop;

        -- Enter bad code -----------------------------------------------------
        -- Request start sending code
        start <= '1';

        -- Wait start acknowledge
        wait until dack = '1' for 2 * CLK_PERIOD;
        assert dack = '1'
            report "[FAILURE]: acknowledge failed."
            severity failure;

        -- Check DONE signal deasserted
        assert done = '0'
            report "[FAILURE]: DONE should have been deasserted by now."
            severity failure;
        -- Check MATCH deasserted
        assert match = '0'
            report "[FAILURE]: MATCH should have been deasserted by now."
            severity failure;

        -- Wait start acknowledge deassertion
        start <= '0';
        wait until dack = '0' for 2 * CLK_PERIOD;
        assert dack = '0'
            report "[FAILURE]: deassert start acknowledge failed."
            severity failure;

        -- Send code digit by digit
        for i in BAD_CODE'range loop
            -- Send digit
            digit <= BAD_CODE(i);
            drdy  <= '1';
 
            -- Wait digit acknowledge
            wait until dack = '1' for 2 * CLK_PERIOD;
            assert dack = '1'
                report "[FAILURE]: digit acknowledge failed."
                severity failure;
 
            -- Wait digit acknowledge deassertion
            drdy <= '0';
            wait until dack = '0' for 2 * CLK_PERIOD;
            assert dack = '0'
                report "[FAILURE]: deassert digit acknowledge failed."
                severity failure;
        end loop;

        -- Check DONE signal asserted
        wait until done = '1' for 2 * CLK_PERIOD;
        assert done = '1'
            report "[FAILURE]: comparision should be finished by now."
            severity failure;
        -- Check good code signaled.
        assert match = '0'
            report "[FAILURE]: comparision should have failed."
            severity failure;
        ----------------------------------------------------- Enter bad code --

        wait until Clk = '1';    
        assert false
            report "[SUCCESS]: simulation finished."
            severity failure;
    end process;
end tb;


library ieee;
use ieee.std_logic_1164.all;

use work.aux_types.all;

entity TOP_TB is
end TOP_TB;

architecture TB of TOP_TB is
    component TOP
        generic (
            CLKIN_FREQ : positive;
            CODE       : code_vector
        );
        port (
            CLK100MHZ  : in    std_logic;                     -- Nexys4 DDR 100 MHz clock
            CPU_RESETN : in    std_logic;                     -- Nexys4 DDR reset button
            SW         : in    std_logic_vector(5 downto 0);  -- Nexys4 DDR switches
            LED        : out   std_logic_vector(2 downto 0)   -- Nexys4 DDR LEDs
        );
    end component;

    -- Inputs
    signal clk100mhz  : std_logic;
    signal sw         : std_logic_vector(5 downto 0);
    signal cpu_resetn : std_logic;

    -- Outputs
    signal led     : std_logic_vector(2 downto 0);

    -- Aliases
    alias DIGIT : std_logic_vector(3 downto 0) is sw(3 downto 0);
    alias DRDY  : std_logic is sw(4);
    alias START : std_logic is sw(5);
    alias DACK  : std_logic is LED(0);
    alias DONE  : std_logic is LED(1);
    alias MATCH : std_logic is LED(2);

    constant CLK_FREQ   : positive := 400;  -- 400 Hz
    constant CLK_PERIOD : time := 1 sec / CLK_FREQ;
    constant CODE       : code_vector := ("0001", "0010", "0011", "0100");
begin
    uut: TOP
        generic map (
            CLKIN_FREQ => CLK_FREQ,
            CODE       => CODE 
        )
        port map(
            CLK100MHZ  => clk100mhz,
            CPU_RESETN => cpu_resetn,
            SW         => sw,
            LED        => led
        );

    clkgen : process
    begin
        clk100mhz <= '0';
        wait for 0.5 * CLK_PERIOD;
        clk100mhz <= '1';
        wait for 0.5 * CLK_PERIOD;
    end process;

    stimuli: process
    begin
        -- Reset
        wait for 2.25 * CLK_PERIOD;
        cpu_resetn <= '0';
        wait for 5.5 * CLK_PERIOD;
        cpu_resetn <= '1';

        -- Enter code ----------------------------------------------------
        -- Request start sending code
        start <= '1';

        -- Wait start acknowledge
        wait until dack = '1' for 100 * CLK_PERIOD;
        assert dack = '1'
            report "[FAILURE]: acknowledge failed."
            severity failure;

        -- Wait start acknowledge deassertion
        start <= '0';
        wait until dack = '0' for 100 * CLK_PERIOD;
        assert dack = '0'
            report "[FAILURE]: deassert start acknowledge failed."
            severity failure;

        -- Send code digit by digit
        for i in CODE'range loop
            -- Send digit
            digit <= CODE(i);
            drdy  <= '1';
 
            -- Wait digit acknowledge
            wait until dack = '1' for 100 * CLK_PERIOD;
            assert dack = '1'
                report "[FAILURE]: digit acknowledge failed."
                severity failure;
 
            -- Wait digit acknowledge deassertion
            drdy <= '0';
            wait until dack = '0' for 100 * CLK_PERIOD;
            assert dack = '0'
                report "[FAILURE]: deassert digit acknowledge failed."
                severity failure;
        end loop;

        -- Check DONE signal asserted
        wait until done = '1' for 100 * CLK_PERIOD;
        assert done = '1'
            report "[FAILURE]: comparision should be finished by now."
            severity failure;
        -- Check good code signaled.
        assert match = '1'
            report "[FAILURE]: comparision failed."
            severity failure;            
        ---------------------------------------------------- Enter good code --

        assert false
            report "[SUCCESS]: simulation finished."
            severity failure;
    end process;
end TB;

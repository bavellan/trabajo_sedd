library ieee;
use ieee.std_logic_1164.all;

entity tb_Mquina is
end tb_Mquina;

architecture tb of tb_Mquina is

    component Mquina
        port (Comparado : in std_logic_vector (1 downto 0);
              Puerta    : in std_logic;
              Rst       : in std_logic;
              Clk       : in std_logic;
              Estado    : out std_logic_vector (2 downto 0);
              Aviso     : out std_logic
              );
    end component;

    signal Comparado : std_logic_vector (1 downto 0);
    signal Puerta    : std_logic;
    signal Rst       : std_logic;
    signal Clk       : std_logic;
    signal Estado    : std_logic_vector (2 downto 0);
    signal Aviso     : std_logic;

begin

    dut : Mquina
    port map (Comparado => Comparado,
              Puerta    => Puerta,
              Rst       => Rst,
              Clk       => Clk,
              Estado    => Estado,
              Aviso     => Aviso);

   clk_n_process :process
  begin
    Clk <= '0';
    wait for 0.5 us;
    Clk <= '1';
    wait for 0.5 us;
  end process;
    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        Comparado <= "00";
        Puerta <= '0';
        Rst<='0';
        wait for 1 us;

        Comparado <= "10";
       -- Puerta <= '0';
        Rst<='1';
        wait for 1 us;   
        Puerta <= '1';
        Rst<='1';
        wait for 1 us;
        
        Comparado <= "00";
        Puerta <= '0';
        Rst<='1';
        wait for 1 us;
        
        Comparado <= "11";
       -- Puerta <= '0';
        Rst<='1';
        wait for 0.5 us;
        Comparado<="00";
        wait for 0.5 us;
  
       
        Comparado <= "11";
       -- Puerta <= '0';
        Rst<='1';
        wait for 1 us;
        Rst<='1';
        wait for 1 us;
        Rst<='1';
        wait for 0.2 us;
        Comparado<="00";
        wait for 0.5 us;
        Comparado<="11";
        
         wait for 1 us;
        Comparado<="00";
        
        wait for 0.5 us;
        Comparado<="10";
        
        wait for 2 us;
        Comparado<="00";
        Puerta<='1';
        wait for 1 us;
        Puerta<='0';
        wait for 1 us;
        Comparado<="11";
        wait for 0.5 us;
        Comparado<="00";
        wait for 0.5 us;
        Comparado<="11";
        wait for 0.5 us;
        Comparado<="00";
        wait for 0.5 us;
        Comparado<="11";
        wait for 0.5 us;
        Comparado<="00";--Pasamos al puk
        wait for 0.5 us;
        Comparado<="11";
        wait for 0.5 us;
        Comparado<="00";
        wait for 0.5 us;
        Comparado<="11";
          wait for 0.5 us;
        Comparado<="11";
        wait for 0.5 us;
        Comparado<="00";
         wait for 0.5 us;
        Comparado<="11";
        wait for 2 us;
        Rst<='0';
        wait;
    end process;

end tb;



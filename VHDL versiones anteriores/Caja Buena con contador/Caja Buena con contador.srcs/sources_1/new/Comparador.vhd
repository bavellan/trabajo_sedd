library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Comparador is
generic(D1: std_logic_vector(3 downto 0):="0001";
        D2: std_logic_vector(3 downto 0):="0010";
        D3: std_logic_vector(3 downto 0):="0011";
        D4: std_logic_vector(3 downto 0):="0100");
 Port ( 
 Digitos :      in std_logic_vector(3 downto 0);
 SigDig:        in std_logic;
 Clk:           in std_logic;
 Comparacion:   out std_logic_vector(1 downto 0)
 );
end Comparador;
architecture Behavioral of Comparador is
signal Reg_Digitos: std_logic_vector(3 downto 0);
signal ContraFallada: std_logic:='0';
signal TresFalos: std_logic:='0';
shared variable CuentaIntentos, CuentaNumeros:  integer range 0 to 3:=0;
begin
Reg_Digitos<=Digitos;
Compara: process(clk)
begin
end process;
end Behavioral;

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
library xil_defaultlib;


entity i2c_leds is
  port (
    scl              : inout std_logic;
    sda              : inout std_logic;
    clk              : in    std_logic;
    rst              : in    std_logic
  --  led_o            : out std_logic_vector(7 downto 0)
  );
end entity;

architecture RTL of i2c_leds is
component I2CFuncion
  port (
    scl              : inout std_logic;
    sda              : inout std_logic;
    clk              : in    std_logic;
    rst              : in    std_logic;
    read_req        : out std_logic;
    data_to_master   : std_logic_vector(7 downto 0);
    data_valid      : out std_logic;
    data_from_master: out std_logic_vector(7 downto 0)
    );
end component;

signal read_req         : std_logic;
signal data_to_master   : std_logic_vector(7 downto 0);
signal data_valid       : std_logic;
signal data_from_master : std_logic_vector(7 downto 0);
shared variable counter, counter2 : integer range 0 to 3 :=0;

signal data_reg : std_logic_vector(7 downto 0);--Guarda la entrada del micro

signal flag: std_logic:='0';
TYPE digit is (D0, D1,D2,D3,D4);
    signal dig, next_dig:digit;
    
begin

 --i2c_slave0 : entity work.I2CFuncion(arch)port map(scl,sda,clk,rst, read_req, data_to_master,data_valid,data_from_master);
inst_i2cfuncion: I2CFuncion port map(--esto equivale a la linea de arriba se supone
    scl=>scl,
    sda=>sda,
    clk=>clk,
    rst=>rst,
    
    read_req=>read_req,
    data_to_master=>data_to_master,
    data_valid=>data_valid,
    data_from_master=>data_from_master
);
 -- led_o          <= data_reg;
--  data_to_master <= data_reg;--manda el valor de la fpga al micro

  process(clk)
  begin
  if rst = '0' then
        dig <= D0;
         counter:=0;
        counter2:=0;
  elsif(clk'event and clk='1') then
        dig <= next_dig;
      if(data_valid='1') then 
        data_reg <= data_from_master;--si se percibe dato se guarda en reg
      end if;        
    end if;			 
  end process;
   nxt_state_decoder: process (dig, data_reg)--maquina de estados
  begin
  if counter<3 then
    next_dig<= dig;--el contador aumenta si el digito percibido es distinto al ejemplo de 
    --digitos de contraseņa---1234---
    case dig is
      when D0 =>
        if data_reg <= "000000001" then
          next_dig <= D1; -- 1---
          else
          counter:=counter+1;
           next_dig <= D0;
        end if;
      when D1 =>
        if data_reg <= "000000010" then    -- 12--
          next_dig <= D2;
          else
          counter:=counter+1;
           next_dig <= D0;
        end if;
      when D2 =>
        if data_reg <= "000000011" then    -- 123-
          next_dig <= D3;
        else                
        counter:=counter+1;
          next_dig <= D0;
        end if;
      when D3 =>
        if data_reg <= "00000100" then    -- 1234-
          next_dig <= D4;
        else             
         counter:=counter+1;   
         next_dig <= D0;
        end if;
      when D4 =>
       data_to_master<="00000001";--si la secuencia es correcta manda un 1 a la micro 
    end case;
    
    
    else
        if flag<='0'then
            data_to_master<="00000000";--nos equivocamos 3 veces, seceunrcia incorrecta y vamos al puk; 
            dig<=D0;--Volvemos al estado inicial   
            flag<='1';-- flag a 1 para que al reiniciar el bucle no velva a mandar 0 a la micro
        end if;
        
   if counter2<3 then     
   next_dig<= dig;--el contador aumenta si el digito percibido es distinto al ejemplo de 
         --digitos de contraseņa---1234--- pero esta vez del puk
   case dig is
     when D0 =>
        if data_reg <= "000000001" then
          next_dig <= D1; -- 1---
          else
          counter2:=counter2+1;
           next_dig <= D0;
        end if;
      when D1 =>
        if data_reg <= "000000010" then    -- 12--
          next_dig <= D2;
          else
          counter2:=counter2+1;
           next_dig <= D0;
        end if;
      when D2 =>
        if data_reg <= "000000011" then    -- 123-
          next_dig <= D3;
        else                
        counter2:=counter2+1;
          next_dig <= D0;
        end if;
      when D3 =>
        if data_reg<= "00000100" then    -- 1234-
          next_dig <= D4;
        else             
         counter2:=counter2+1;   
         next_dig <= D0;
        end if;
       when D4 =>
       data_to_master<="00000001";--si la secuencia es correcta manda un 1 a la micro 
       end case;
    else
     data_to_master<="00000000";--puk incorrecta
     next_dig <= D0;
     end if;
   end if;
   if counter<=3 and counter2<=3 and data_reg<= "00000000" then --reinicia todo para volver a empezar
   --siempre que se reciba 0;
   counter:=0;
   counter:=0;
   flag<='1';
   end if;
  end process;
end architecture;

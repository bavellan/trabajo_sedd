----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.02.2021 02:13:39
-- Design Name: 
-- Module Name: comparador_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity comparador_tb is
--  Port ( );
end comparador_tb;

architecture Behavioral of comparador_tb is

component comparador
port (digitos : in STD_LOGIC_VECTOR (3 downto 0);
      comparacion : out STD_LOGIC_VECTOR (1 downto 0);
      clk : in STD_LOGIC;
 --     rst : in STD_LOGIC;
      sigdig: in std_logic;
      aux : out std_logic_vector(3 downto 0)
);
end component;
signal digitos : std_logic_vector (3 downto 0);
signal comparacion : std_logic_vector (1 downto 0):="00";
signal clk : std_logic;
--signal rst : std_logic;
signal sigdig : std_logic;
signal aux : std_logic_vector(3 downto 0);

begin
uut: comparador port map(
    digitos => digitos,
    comparacion => comparacion,
    clk => clk,
 --   rst => rst,
    sigdig => sigdig,
    aux => aux
);

digit: process
begin
  --              rst <='1';
    digitos<="0000";        --fallo inicial
    wait for 10 ns;             
    digitos<="1001";
    wait for 60 ns;             
    digitos<="0101";
    wait for 60 ns;             
    digitos<="0011";
    wait for 60 ns;             
    digitos<="0100";                --lobby 210
 --   wait for 60 ns;             
 --   digitos<="1000";
    
    wait for 60 ns;            --fallo mitad
    digitos<="0001";
    wait for 60 ns;             
    digitos<="0010";
    wait for 60 ns;             
    digitos<="0010";
    wait for 60 ns;             
    digitos<="0110";
    wait for 60 ns;             
    digitos<="1000";
    wait for 60 ns;                 --pal lobby 570
    
    
    
    wait for 60 ns;             --fallo final
    digitos<="0001";            
    wait for 60 ns;             
    digitos<="0010";
    wait for 60 ns;             
    digitos<="0011";
    wait for 60 ns;             
    digitos<="0110";
    wait for 60 ns;             
    digitos<="1000";
    wait for 60 ns;             
    digitos<="0110";
    wait for 60 ns;                 --pal lobby, pero no se ha ido, deberia estar en el lobby de 1000 a 1020
    digitos<="1000";                --otra opcion es magia negra en el tercer fallo y el lobby coincide con el flanco negativo del clk y del tercer intento
    wait for 60 ns;                 --tampoco deberia estar este wait
    
    wait for 60 ns;             --sin fallo
    digitos<="0001";            
    wait for 60 ns;             
    digitos<="0010";
    wait for 60 ns;             
    digitos<="0011";
    wait for 60 ns;             
    digitos<="0100";
    wait for 60 ns;             
    digitos<="1000";
    wait for 300 ns;            --separar
    
    
    
 --                rst <='0';
    digitos<="0000";        --fallo inicial
    wait for 10 ns;             
    digitos<="1001";
    wait for 60 ns;             
    digitos<="0101";
    wait for 60 ns;             
    digitos<="0011";
    wait for 60 ns;             
    digitos<="0100";                --lobby 210
 --   wait for 60 ns;             
 --   digitos<="1000";
    
    wait for 60 ns;            --fallo mitad
    digitos<="0001";
    wait for 60 ns;             
    digitos<="0010";
    wait for 60 ns;             
    digitos<="0010";
    wait for 60 ns;             
    digitos<="0110";
    wait for 60 ns;             
    digitos<="1000";
    wait for 60 ns;                 --pal lobby 570
    
    
    
    wait for 60 ns;             --fallo final
    digitos<="0001";            
    wait for 60 ns;             
    digitos<="0010";
    wait for 60 ns;             
    digitos<="0011";
    wait for 60 ns;             
    digitos<="0110";
    wait for 60 ns;             
    digitos<="1000";
    wait for 60 ns;             
    digitos<="0110";
    wait for 60 ns;                 --pal lobby, pero no se ha ido, deberia estar en el lobby de 1000 a 1020
    digitos<="1000";                --otra opcion es magia negra en el tercer fallo y el lobby coincide con el flanco negativo del clk y del tercer intento
    wait for 60 ns;                 --tampoco deberia estar este wait
    
    wait for 60 ns;             --sin fallo
    digitos<="0001";            
    wait for 60 ns;             
    digitos<="0010";
    wait for 60 ns;             
    digitos<="0011";
    wait for 60 ns;             
    digitos<="0100";
    wait for 60 ns;             
    digitos<="1000";
    wait for 300 ns;            --separar
    
end process;

sig: process
begin
    sigdig<='0';
    wait for 30 ns;
    sigdig<='1';
    wait for 30 ns;
end process;


p_clk : process
begin
    clk <='0';
    wait for 20 ns;
    clk <='1';
    wait for 20 ns;
end process;

end Behavioral;






















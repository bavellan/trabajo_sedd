----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.02.2021 00:28:59
-- Design Name: 
-- Module Name: comparador - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity comparador is
generic (D0 : STD_LOGIC_VECTOR (3 downto 0) :="0001";
         D1 : STD_LOGIC_VECTOR (3 downto 0) :="0010";
         D2 : STD_LOGIC_VECTOR (3 downto 0) :="0011";
         D3 : STD_LOGIC_VECTOR (3 downto 0) :="0100";
         Daux : std_logic_vector(3 downto 0) := "1111" );
Port (   digitos : in STD_LOGIC_VECTOR (3 downto 0);
         comparacion : out STD_LOGIC_VECTOR (1 downto 0);
         clk : in STD_LOGIC;
         rst : in std_logic :='1';
         sigdig: in std_logic;
         aux : out std_logic_vector(3 downto 0) :="0000"
         );
end comparador;

architecture Behavioral of comparador is

--signal pass_fail : std_logic :='0';
--signal tres_fallos : std_logic :='0';
shared variable cuenta_intentos : integer range 0 to 4 :=0;
shared variable cuenta_numeros : integer range 0 to 3 :=0;
signal Dauxs : std_logic_vector(3 downto 0) :="0000";
signal Dauxs2 : std_logic_vector(3 downto 0) :="0000";

begin
comp: process(clk,sigdig,digitos)
begin
 comparacion(1)<='0';
 comparacion(0)<='0';
 --while rst = '1' loop
 --if rst ='1' then
  --  if(rising_edge(clk)) then
    if cuenta_intentos <4 then --empieza a leer la contraseņa para comprobar si esta bien
            if rising_edge(sigdig) then
                if digitos = D0 or Dauxs(0)=Daux(0) then
                    cuenta_numeros := cuenta_numeros +1;
   --                 aux<="0001";
                    Dauxs(0) <='1';
                        if digitos = D1 or Dauxs(1)=Daux(1) then
                            cuenta_numeros := cuenta_numeros +1;
    --                        aux<="0010";
                            Dauxs(1)<='1';
                                if digitos = D2 or Dauxs(2)=Daux(2) then
                                    cuenta_numeros := cuenta_numeros +1;
     --                               aux<="0011";
                                    Dauxs(2)<='1';
                                        if digitos = D3 then
                                            cuenta_numeros := cuenta_numeros +1;
      --                                      aux<="0100";
       --                                     comparacion(1)<='1';
      --                                      comparacion(0)<='0';
                                            Dauxs(3)<='1';
                                        else
                                            aux<="1111";
       --                                     cuenta_intentos := cuenta_intentos +1;
                                        end if;
                                else
                                    aux<="1110";
        --                            cuenta_intentos := cuenta_intentos +1;
                                end if;
                        else
                            aux<="1101";
      --                      cuenta_intentos := cuenta_intentos +1;
                        end if;
                else
                    aux<="1100";
     --               cuenta_intentos := cuenta_intentos +1;
                end if;
                --aqui prro
                case Dauxs is
     --               when "0000" =>
                    when "0001" =>
                        if Dauxs2(0) ='0' then
                            aux <="0001";
                            Dauxs2(0) <='1';
                        else
                            cuenta_intentos := cuenta_intentos +1;
                        end if;
                    when "0011" =>
                        if Dauxs2(1)='0' then
                            aux <="0010";
                            Dauxs2(1)<='1';
                        else
                            cuenta_intentos := cuenta_intentos +1;
                        end if;
                    when "0111"=>
                        if Dauxs2(2)='0' then
                            aux <="0011";
                            Dauxs(2)<='1';
                        else
                            cuenta_intentos := cuenta_intentos +1;
                        end if;
                    when "1111"=>
                        if Dauxs2(3)='0' then
                            aux<="0100";
                            comparacion(1)<='1';
                            comparacion(0)<='0';
                            Dauxs<="0000";
                            Dauxs2<="0000";
                            cuenta_intentos :=0;
                        else 
                            cuenta_intentos := cuenta_intentos +1;
                        end if;
                    when others =>
                        cuenta_intentos := cuenta_intentos +1;
                end case;
            end if;
            --if rising_edge(clk) then
                
            --end if;
    else --ya han pasado los tres intentos y se avisa a la maquina de estados
        aux<="1011";
        cuenta_intentos :=0;
        comparacion(1)<='1';
        comparacion(0)<='1';
        Dauxs<="0000";
    end if;
--end loop;
--else
 --   comparacion(1)<='0';
 --   comparacion(0)<='0';
 --   cuenta_intentos := 0;
 --   cuenta_numeros := 0;
 --   aux <="0000";
 --   Dauxs<="0000";

--end if;
            
 --   end if;
    
end process;












end Behavioral;













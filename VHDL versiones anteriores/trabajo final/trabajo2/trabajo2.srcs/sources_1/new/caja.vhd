----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.02.2021 00:28:59
-- Design Name: 
-- Module Name: caja - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity caja is
    Port ( comparado : in STD_LOGIC_VECTOR (1 downto 0);
           puerta : in STD_LOGIC;
           rst : in STD_LOGIC;
           clk : in STD_LOGIC;
           estado : out STD_LOGIC_VECTOR (2 downto 0);
           aviso : out STD_LOGIC);
end caja;

architecture Behavioral of caja is

begin


end Behavioral;

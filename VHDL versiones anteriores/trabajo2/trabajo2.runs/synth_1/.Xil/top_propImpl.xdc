set_property SRC_FILE_INFO {cfile:{D:/Usuario/Desktop/Uni/Semestre 7/sed/trabajo/vhdl/trabajo2/trabajo2.srcs/constrs_1/imports/new/Nexys-4-DDR-Master.xdc} rfile:../../../trabajo2.srcs/constrs_1/imports/new/Nexys-4-DDR-Master.xdc id:1} [current_design]
set_property src_info {type:XDC file:1 line:7 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict { PACKAGE_PIN E3    IOSTANDARD LVCMOS33 } [get_ports { clk }]; #IO_L12P_T1_MRCC_35 Sch=clk100mhz
set_property src_info {type:XDC file:1 line:83 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict { PACKAGE_PIN N17   IOSTANDARD LVCMOS33 } [get_ports { rst }]; #IO_L9P_T1_DQS_14 Sch=btnc
set_property src_info {type:XDC file:1 line:84 export:INPUT save:INPUT read:READ} [current_design]
set_property -dict { PACKAGE_PIN M18   IOSTANDARD LVCMOS33 } [get_ports { pushbutton }]; #IO_L4N_T0_D05_14 Sch=btnu

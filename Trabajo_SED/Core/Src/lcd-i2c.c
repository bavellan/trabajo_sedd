/*
 * lcd-i2c.c
 *
 *  Created on: 10 ene. 2021
 *      Author: Daniel
 */

#include "lcd-i2c.h"
#include "main.h"

extern I2C_HandleTypeDef hi2c1;

////////////////////////////////////
//Manda un comando a la pantalla lcd
////////////////////////////////////
void lcd_send_cmd (char cmd)
{
  char data_u, data_l;
	uint8_t data_t[4];
	data_u = (cmd&0xf0);
	data_l = ((cmd<<4)&0xf0);
	data_t[0] = data_u|0x0C;  //en=1, rs=0
	data_t[1] = data_u|0x08;  //en=0, rs=0
	data_t[2] = data_l|0x0C;  //en=1, rs=0
	data_t[3] = data_l|0x08;  //en=0, rs=0
	HAL_I2C_Master_Transmit (&hi2c1, SLAVE_ADDRESS_LCD,(uint8_t *) data_t, 4, 100);
}

///////////////////////////////////////////////////////
//Manda un caracter para que la pantalla lcd lo imprima
///////////////////////////////////////////////////////
void lcd_send_data (char data)
{
	char data_u, data_l;
	uint8_t data_t[4];
	data_u = (data&0xf0);
	data_l = ((data<<4)&0xf0);
	data_t[0] = data_u|0x0D;  //en=1, rs=1
	data_t[1] = data_u|0x09;  //en=0, rs=1
	data_t[2] = data_l|0x0D;  //en=1, rs=1
	data_t[3] = data_l|0x09;  //en=0, rs=1
	HAL_I2C_Master_Transmit (&hi2c1, SLAVE_ADDRESS_LCD,(uint8_t *) data_t, 4, 100);
}

///////////////////////////////////////
//Cambia a la linea determinada (1 o 2)
///////////////////////////////////////
void lcd_change_line(int gotoline)
{
	lcd_send_cmd(0x02);//Devuelve el cursor al principio
	line = 1;

	if (gotoline == 2){
		for (int i=0; i<40; i++){
			HAL_Delay(1);
			lcd_send_cmd(0x14);//Desplaza el cursor a la derecha 40 veces, provocando que cambie de linea
		}
		line = 2;
	}
}

////////////////////////////////////////////////////////////////////
//Manda una cadena de caracteres para que la pantalla lcd la imprima
////////////////////////////////////////////////////////////////////
void lcd_send_string (char *cad)
{
	//lcd_send_cmd(0x01);//Limpia la pantalla
	if (line==1){
		lcd_clear(1);
	}
	if (line==2){
		lcd_clear(2);
	}
	HAL_Delay(10);
	linecounter=0;
	while (*cad){
		linecounter++;
		if (linecounter > 16){//Si se escribe sobre 16 espacios se ocupa la linea entera, por lo que hay que moverla un puesto a la izquierda
			lcd_send_cmd(0x18);
			HAL_Delay(300);
			linecounter--;
		}
		lcd_send_data(*cad++);
		HAL_Delay(100);
	}
}

////////////////////////////
//Inicilaiza la pantalla lcd
////////////////////////////
void lcd_init (void)
{
	// 4 bit initialisation
		HAL_Delay(50);  // wait for >40ms
		lcd_send_cmd (0x30);
		HAL_Delay(5);  // wait for >4.1ms
		lcd_send_cmd (0x30);
		HAL_Delay(1);  // wait for >100us
		lcd_send_cmd (0x30);
		HAL_Delay(10);
		lcd_send_cmd (0x20);  // 4bit mode
		HAL_Delay(10);

	  // display initialisation
		lcd_send_cmd (0x2F); // Function set --> DL=0 (4 bit mode), N = 1 (2 line display) F = 0 (5x8 characters)
		HAL_Delay(1);
		lcd_send_cmd (0x08); //Display on/off control --> D=0,C=0, B=0  ---> display off
		HAL_Delay(1);
		lcd_send_cmd (0x01);  // clear display
		HAL_Delay(1);
		HAL_Delay(1);
		lcd_send_cmd (0x06); //Entry mode set --> I/D = 1 (increment cursor) & S = 0 (no shift)
		HAL_Delay(1);
		lcd_send_cmd (0x0C); //Display on/off control --> D = 1, C and B = 0. (Cursor and blink, last two bits)
}

//////////////////////////////////////////////////////////////
//Limpia la linea determinada (1 o 2) o la pantalla entera (0)
//////////////////////////////////////////////////////////////
void lcd_clear(int line){
	switch (line){
		case 0:
			lcd_send_cmd(0x01);
			break;

		case 1:
			lcd_change_line(1);//Devuelve el cursor al principio de la primera linea
			for (int i=0; i<40; i++){
				lcd_send_data(' ');
			}
			HAL_Delay(1);
			lcd_change_line(1);//Devuelve el cursor al principio de la primera linea
			break;

		case 2:
			lcd_change_line(2);//Devuelve el cursor al principio de la segunda linea
			for (int i=0; i<40; i++){
				lcd_send_data(' ');
			}
			HAL_Delay(1);
			lcd_change_line(2);//Devuelve el cursor al principio de la segunda linea
			break;
	}
}

/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "lcd-i2c.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define SLAVE_ADDRESS_LCD 0x4E
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

I2C_HandleTypeDef hi2c1;

TIM_HandleTypeDef htim1;

/* USER CODE BEGIN PV */
int buttonflag=0, flagmatch=0, flagdone=0, flagdack=0;
int counter=0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM1_Init(void);
/* USER CODE BEGIN PFP */
int CheckNo();//Comprueba el numero dado por el potenciometro
void SendNo(int num);//Envia un numero entero en formato binario desde las salidas PC12, PD1, PD3 y PD5
void Lock();//Bloquea el cerrojo
void Unlock();//Desbloquea el cerrojo
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
uint8_t Analog=0;
int pin[4];

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_ADC1_Init();
  MX_TIM1_Init();
  /* USER CODE BEGIN 2 */

  //inicializacion de salidas
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_7, 0);//No manda nignun digito
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_12, 0);
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_1, 0);
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_3, 0);
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_5, 0);//Pone el digito a 0
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, 0);//Pone el aviso de inicio a 0

  Lock();//Bloquea el cerrojo

  lcd_init();//Inicializa la pantalla lcd
  lcd_change_line(1);
  lcd_send_string("Trabajo SED 2021");
  lcd_change_line(2);
  lcd_send_string("Caja fuerte");
  HAL_Delay(500);
  lcd_clear(0);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

	  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, 1);//Se le envia a la FPGA el aviso de que se debe comenzar

	  flagdack=(HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_2));
	  while(!flagdack){
	  		flagdack=(HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_2));
	  };//Espera a que la FPGA envíe que ha recibido la señal de inicio
	  flagdack=0;

	  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, 0);//Se quita el aviso de que se debe comenzar

	  Lock();//En este estado la caja esta cerrada
	  lcd_clear(0);//Limpia toda la pantalla
	  lcd_change_line(1);
	  if (counter<3)//Mientras el contador esté entre 0 y 2, se comprueba el PIN
		  lcd_send_string("Introducir PIN");
	  else if (counter==3)//Si el contador es 3, se comprueba el PUK
		  lcd_send_string("Introducir PUK");
	  HAL_Delay(1000);
	  lcd_change_line(2);
	  for (int i=0; i<4; i++){
		  //Introcuccion del digito
		  buttonflag=0;//Se resetea el valor del flag del boton
		  while(buttonflag!=1){
			  lcd_send_data(CheckNo()+'0');//Imprime el valor actual del potenciometro
			  HAL_Delay(500);
			  lcd_send_cmd(0x10);//Mueve el cursor a la izquierda
			  lcd_send_data(' ');//Borra el valor actual del potenciometro
			  HAL_Delay(500);
			  lcd_send_cmd(0x10);//Mueve el cursor a la izquierda
		  };
		  buttonflag=0;//Se resetea el valor del flag del boton
		  pin[i] = CheckNo();
		  lcd_send_data(pin[i]+'0');//Imprime el valor escogeido por el usuario
		  if (!flagdone)//En caso de que no se haya decidido ya que la contraseña es incorrecta, se le envía el número a la FPGA
			  SendNo(pin[i]);//Envia el numero en formato binario a la FPGA
		  HAL_Delay(100);
		  flagdone=HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_0);
		  HAL_Delay(1000);
		  buttonflag=0;//Reset del flag
	  }

	  flagdone=HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_0);
	  while(!flagdone){
		  flagdone=HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_0);
	  };
	  flagdone=0;
	  HAL_Delay(100);
	  flagmatch=HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_4);

	  if(!flagmatch){
		  lcd_clear(0);//Limpia toda la pantalla
		  lcd_change_line(1);
		  if (counter < 3)//Mientras el contador esté entre 0 y 2, se comprueba el PIN
			  lcd_send_string("PIN incorrecto");
		  else if (counter == 3){//Si el contador es 3, se comprueba el PUK
			  lcd_send_string("PUK incorrecto");
			  lcd_change_line(2);
			  lcd_send_string("Reinicie el sistema");
			  while(1){}//El código se detiene, por lo que es necesario resetear para salir del bucle
		  }
		  counter++;//En caso de fallo se incrementa el contador
		  lcd_change_line(2);
		  lcd_send_string("Vuelva a pulsar el boton");

		  buttonflag=0;//Se resetea el valor del flag del boton
		  while(!buttonflag){};//Se espera a que se pulse el boton
		  buttonflag=0;//Se resetea el valor del flag del boton
	  }
	  else{
		  flagmatch=0;
		  lcd_clear(0);//Limpia toda la pantalla
		  lcd_change_line(1);
		  if (counter < 3)//Mientras el contador esté entre 0 y 2, se comprueba el PIN
		  	  lcd_send_string("PIN correcto");
		  else if (counter == 3)//Si el contador es 3, se comprueba el PUK
			  lcd_send_string("PUK correcto");
		  lcd_change_line(2);
		  lcd_send_string("Abriendo puerta");
		  Unlock();
		  counter=0;//Se resetea el contador

		  HAL_Delay(1000);
		  lcd_clear(0);//Limpia toda la pantalla
		  lcd_change_line(1);
		  lcd_send_string("Una vez termine, cierre");
		  lcd_change_line(2);
		  lcd_send_string("y pulse el boton");

		  buttonflag=0;//Se resetea el valor del flag del boton
		  while(!buttonflag){};//Se espera a que se pulse el boton
		  buttonflag=0;//Se resetea el valor del flag del boton

		  lcd_clear(0);//Limpia toda la pantalla
		  lcd_change_line(1);
		  lcd_send_string("Caja cerrada");

	  }

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
  hadc1.Init.Resolution = ADC_RESOLUTION_8B;
  hadc1.Init.ScanConvMode = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_1;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 160;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 2000;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_12, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_1|GPIO_PIN_3|GPIO_PIN_5|GPIO_PIN_7, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, GPIO_PIN_RESET);

  /*Configure GPIO pin : PA0 */
  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PC12 */
  GPIO_InitStruct.Pin = GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PD0 PD2 PD4 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_2|GPIO_PIN_4;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : PD1 PD3 PD5 */
  GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_3|GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : PD7 */
  GPIO_InitStruct.Pin = GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : PB4 */
  GPIO_InitStruct.Pin = GPIO_PIN_4;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

}

/* USER CODE BEGIN 4 */
HAL_GPIO_EXTI_Callback(uint16_t Pin){
	if(Pin==GPIO_PIN_0){
		buttonflag=1;
	}
}

int CheckNo(){
	HAL_ADC_Start(&hadc1);//Cominezo
	if (HAL_ADC_PollForConversion(&hadc1, HAL_MAX_DELAY)==HAL_OK){
	 Analog=HAL_ADC_GetValue(&hadc1);
	}
	HAL_ADC_Stop(&hadc1);

	if(Analog<=20)//Si el valor analogico esta entre 0 y 25 devuelve un 0
		return 0;
	else if(Analog <= 41)//Si el valor analogico esta entre 26 y 51 devuelve un 1
		return 1;
	else if(Analog <= 72)//Si el valor analogico esta entre 52 y 76 devuelve un 2
		return 2;
	else if(Analog <= 90)//Si el valor analogico esta entre 77 y 102 devuelve un 3
		return 3;
	else if(Analog <= 118)//Si el valor analogico esta entre 103 y 127 devuelve un 4
		return 4;
	else if(Analog <= 146)//Si el valor analogico esta entre 128 y 153 devuelve un 5
		return 5;
	else if(Analog <= 172)//Si el valor analogico esta entre 154 y 179 devuelve un 6
		return 6;
	else if(Analog <= 197)//Si el valor analogico esta entre 180 y 204 devuelve un 7
		return 7;
	else if(Analog <= 224)//Si el valor analogico esta entre 205 y 230 devuelve un 8
		return 8;
	else//Si el valor analogico esta entre 231 y 255 devuelve un 9
		return 9;
}

void SendNo(int num){
	//Usando el metodo de la sucesion sucesiva por 2
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_12, (num%2));//Pin 0
	num=num/2;
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_1, (num%2));//Pin 1
	num=num/2;
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_3, (num%2));//Pin 2
	num=num/2;
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_5, (num));//Pin 3

	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_7, 1);//Se indica a la FPGA que se ha introducido un digito
	flagdack=HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_2);
	while(!flagdack){
		flagdack=(HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_2));
	};//Espera a que la FPGA envíe que ha recibido un número
	flagdack=0;
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_7, 0);//Se vuelve a poner a 0 para permitir los flancos positivos
}

void Lock(){
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);//Comienza la generacion de PWM
	__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, 150);
	HAL_Delay(500);
	HAL_TIM_PWM_Stop(&htim1, TIM_CHANNEL_1);//Detiene la generacion de PWM
}

void Unlock(){
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);//Comienza la generacion de PWM
	__HAL_TIM_SET_COMPARE(&htim1, TIM_CHANNEL_1, 69);
	HAL_Delay(500);
	HAL_TIM_PWM_Stop(&htim1, TIM_CHANNEL_1);//Detiene la generacion de PWM
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

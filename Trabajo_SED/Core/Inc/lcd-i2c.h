/*
 * lcd-i2c.h
 *
 *  Created on: 10 ene. 2021
 *      Author: Daniel
 */

#ifndef INC_LCD_I2C_H_
#define INC_LCD_I2C_H_


#define SLAVE_ADDRESS_LCD 0x4E

	int linecounter;//Contador del numero de espacios que lleva escritos en la linea
	int line;//Numero de linea 1 o 2

	void lcd_send_cmd(char cmd);//Manda un comando a la pantalla lcd
	void lcd_send_data(char data);//Manda un caracter para que la pantalla lcd lo imprima
	void lcd_change_line(int gotoline);//Cambia a la linea determinada (1 o 2)
	void lcd_send_string (char * cad);//Manda una cadena de caracteres para que la pantalla lcd la imprima
	void lcd_init();//Inicilaiza la pantalla lcd
	void lcd_clear(int line);//Limpia la linea determinada (1 o 2) o la pantalla entera (0)

#endif /* INC_LCD_I2C_H_ */

library ieee;
use ieee.std_logic_1164.all;

entity DEBOUNCER_TB is
end DEBOUNCER_TB;

architecture TB of DEBOUNCER_TB is
    component DEBOUNCER
        generic (
            WIDTH   : positive
        );
        port (
            RST_N   : in  std_logic;
            CLK     : in  std_logic;
            ENTRADA : in  std_logic_vector(WIDTH - 1 downto 0);
            SALIDA  : out std_logic_vector(WIDTH - 1 downto 0)
        );
    end component;

    -- Inputs
    signal rst_n   : std_logic;
    signal clk     : std_logic;
    signal entrada : std_logic_vector(0 downto 0);

    -- Outputs
    signal salida  : std_logic_vector(ENTRADA'range);

    constant CLK_PERIOD : time := 10 ns;
    constant INPUT_WAVEFORM : std_logic_vector := "00010101100111111111101110010000000000";

begin
    uut: DEBOUNCER
        generic map (
            WIDTH => ENTRADA'length
        )
        port map (
            RST_N   => rst_n,
            CLK     => clk,
            ENTRADA => entrada,
            SALIDA  => salida
        );

    clkgen: process
    begin
        clk <= '0';
        wait for 0.5 * CLK_PERIOD;
        clk <= '1';
        wait for 0.5 * CLK_PERIOD;
    end process;

    stimuli: process
    begin
        -- Reset
        wait for 0.25 * CLK_PERIOD;
        rst_n <= '0';
        wait for 0.5 * CLK_PERIOD;
        rst_n <='1';
        wait until clk = '1';

        for i in INPUT_WAVEFORM'range loop
            entrada(0) <= INPUT_WAVEFORM(i);
            wait until clk = '1';
        end loop;

        wait until Clk = '1';    
        assert false
            report "[SUCCESS]: simulation finished."
            severity failure;
    end process;

end TB;

-- Asynchronous Input Synchronization
--
-- The following code is an example of synchronizing an asynchronous input
-- of a design to reduce the probability of metastability affecting a circuit.
--
-- The following synthesis and implementation attributes are added to the code
-- in order improve the MTBF characteristics of the implementation:
--
--  ASYNC_REG="TRUE" - Specifies registers will be receiving asynchronous data
--                     input to allow tools to report and improve metastability
--
-- The following constants are available for customization:
--
--   SYNC_STAGES     - Integer value for number of synchronizing registers, must be 2 or higher
--   PIPELINE_STAGES - Integer value for number of registers on the output of the
--                     synchronizer for the purpose of improveing performance.
--                     Particularly useful for high-fanout nets.
--   INIT            - Initial value of synchronizer registers upon startup, 1'b0 or 1'b1.

library ieee;
use ieee.std_logic_1164.all;

entity SYNCHRONIZER is
    generic (
        WIDTH           : positive;
        SYNC_STAGES     : integer := 3;
        PIPELINE_STAGES : integer := 1;
        INIT            : std_logic := '0'
    );
    port (
        CLK      : in  std_logic;
        ASYNC_IN : in  std_logic_vector (WIDTH - 1 downto 0);
        SYNC_OUT : out std_logic_vector (WIDTH - 1 downto 0)
    );
end SYNCHRONIZER;

architecture BEHAVIORAL of SYNCHRONIZER is
begin
    synchros: for i in ASYNC_IN'range generate
        signal sreg : std_logic_vector(SYNC_STAGES - 1 downto 0) := (others => INIT);
        attribute async_reg : string;
        attribute async_reg of sreg : signal is "true";
        signal sreg_pipe : std_logic_vector(PIPELINE_STAGES - 1 downto 0) := (others => INIT);
        attribute shreg_extract : string;
        attribute shreg_extract of sreg_pipe : signal is "false";
    begin
        process(CLK)
        begin
            if rising_edge(CLK) then
                sreg <= sreg(SYNC_STAGES - 2 downto 0) & ASYNC_IN(i);  -- Async Input async_in
            end if;
        end process;

        no_pipeline: if PIPELINE_STAGES = 0 generate
        begin
            SYNC_OUT(i) <= sreg(SYNC_STAGES - 1);
        end generate;

        one_pipeline: if PIPELINE_STAGES = 1 generate
        begin
            process(CLK)
            begin
                if rising_edge(CLK) then
                    SYNC_OUT(i) <= sreg(SYNC_STAGES - 1);
                end if;
            end process;
        end generate;

        multiple_pipeline: if PIPELINE_STAGES > 1 generate
        begin
            process(clk)
            begin
                if rising_edge(CLK) then
                    sreg_pipe <= sreg_pipe(PIPELINE_STAGES - 2 downto 0) & sreg(SYNC_STAGES - 1);
                end if;
            end process;
            SYNC_OUT(i) <= sreg_pipe(PIPELINE_STAGES - 1);
        end generate;
    end generate;
end BEHAVIORAL;

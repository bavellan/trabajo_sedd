library ieee;
use ieee.std_logic_1164.all;

package aux_types is
 type code_vector is array (positive range <>) of std_logic_vector(3 downto 0);
 type code_vector2 is array (positive range <>) of std_logic_vector(3 downto 0);
end package;

library ieee;
use ieee.std_logic_1164.all;


package body aux_types is
end package body;


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


use work.aux_types.all;


entity MQUINA is
    generic (
        CODE : code_vector;             -- Initial password
        CODE_PUK:code_vector2           --PUK password
     );
    port(
        RST_N : in  std_logic;
        CLK   : in  std_logic;
        START : in  std_logic;          -- Request start to send code (from STM)
        DRDY  : in  std_logic;          -- Digit ready to read (from STM)
        DIGIT : in  std_logic_vector(3 downto 0);  -- Digit (from STM)
        DACK  : out std_logic;          -- Digit ready acknowledge (to STM)
        DONE  : out std_logic;          -- Code checking finished (to STM)
        MATCH : out std_logic;          -- Code match (to STM)
        LEDS_COUNTER: out std_logic_vector(3 downto 0);
        DACKrep  : out std_logic;       -- Digit ready acknowledge (to LED)
        DONErep  : out std_logic;       -- Code checking finished (to LED)
        MATCHrep : out std_logic        -- Code match (to LED)
     );                              
end MQUINA;

architecture BEHAVIORAL of MQUINA is
    type MQUINA_STATE is (
        E0_NOMATCH,     -- Initial state / Invalid code
        E1_STARTACK,    -- Acknowledge start request
        E2_WTDIGIT,     -- Wait for digit
        E3_DIGACK,      -- Acknowledge digit
        E4_MATCH        -- Valid code
    );
    signal state, nxt_state : MQUINA_STATE;
    signal index, nxt_index : positive range CODE'range;
    signal index_puk, nxt_index_puk : positive range CODE_PUK'range; -- Add PUK index
    signal counter: integer range 0 to 4:=0;                        -- Add counter
          
begin
--state_reg--
    state_reg: process (RST_N, CLK)
    begin
      
        if RST_N = '0' then                                     --Signals reset
            state <= E0_NOMATCH;
            index <= CODE'low;
            index_puk<=CODE_PUK'low;
            counter<=0;
        elsif rising_edge(clk) then
            if state=E3_DIGACK then                              --counter increment: se aumenta el valor del contador de en flanco de subida, se reiniciara con un reset o al introducir bien la secuencia de d�gitos
                if nxt_state=E0_NOMATCH then
                    counter<=counter+1;   
                end if;
            elsif state=E4_MATCH then 
                counter<=0;
            end if;
            state <= nxt_state;
            index <= nxt_index;
            index_puk<=nxt_index_puk;  
        end if;
    end process;
    --state_reg END--
--next_decoder--
--Comportamiento de la maquina de estados. 
    nxtstate_decoder: process (state, START, DRDY)
 
    begin
        nxt_state <= state;
        nxt_index <= index;
        nxt_index_puk<=index_puk;
        case state is
            when E0_NOMATCH =>                                      -- Estado inicial que espera la se�al de start de la stm para el cambio de estado
                if counter <=3 then                                 -- While PUK ok
                    if START = '1' then                             
                        nxt_state <= E1_STARTACK;
                    end if;   
                else
                         nxt_state<=E0_NOMATCH;
                end if; 
                
            when E1_STARTACK =>
                if counter<3 then
                    nxt_index <= CODE'low;                          -- Restart code digit index: se coloca el puntero en la primera posicion del vector CODE
                    if START = '0' then                             -- En caso de recibir se�al baja de start pasamos a E2_WTDIGIT
                        nxt_state <= E2_WTDIGIT;
                    end if;
                    
                elsif counter=3 then                                -- Counter=3==> Se comparan los digitos introducidos con el puk
                    nxt_index_puk <= CODE_PUK'low;                  -- Restart code digit index del puk: se coloca el puntero en la primera posicion del vector CODE_PUK
                    if START = '0' then
                        nxt_state <= E2_WTDIGIT;
                    end if;
                 end if;
                        

            when E2_WTDIGIT =>
                if START = '1' then
                    nxt_state <= E1_STARTACK;
                elsif DRDY = '1' then
                    nxt_state <= E3_DIGACK;
                end if;
                

            when E3_DIGACK =>
                if counter<3 then                                   --PIN: mientras el contador sea menor que 3, los digitos se comparan con CODE
                    if START = '1' then
                        nxt_state <= E1_STARTACK;
                    
                    elsif DIGIT /= CODE(index) then                 --incorrect digit: digito mal introducido
                   
                        nxt_state <= E0_NOMATCH;                    --Restart to reintroduce digit: se regresa al estado inicial para volver a intentarlo.
                    
                    elsif DRDY = '0' then                           --correct digit : el digito introducido es correcto
                        if index /= CODE'high then
                            nxt_index <= index + 1;                 --increment digit index: se incrementa la poosicion del puntero en uno del vector CODE
                            nxt_state <= E2_WTDIGIT;
                        else
                            nxt_state <= E4_MATCH;
                        end if;
                    end if;
                elsif counter=3 then                                --PUK: con el contador a 3, los digitos se comparan con CODE_PUK
              
                    if START = '1' then
                        nxt_state <= E1_STARTACK;
                    
                    elsif DIGIT /= CODE_PUK(index_puk) then         --incorrect digit: digito mal introduciod, se vuelve al estado inicial a la espera del reset
                    
                        nxt_state <= E0_NOMATCH;                    --Restart to wait for reset
                    
                         
                    elsif DRDY = '0' then                           --correct digit: digito correcto
                        if index_puk /= CODE_PUK'high then
                            nxt_index_puk <= index_puk + 1;         --increment digit index: se incrementa la poosicion del puntero en uno del vector CODE_PUK
                            nxt_state <= E2_WTDIGIT;
                        else
                            nxt_state <= E4_MATCH;
                        end if;
                    end if;
                elsif counter>3 then                                --Counter>3==> Vuelve al estado inicial a la espera del reset de las placas.
                    nxt_state <= E0_NOMATCH;
                end if;

            when E4_MATCH =>                                        --correct Passwords
                if START = '1' then
                 
                    nxt_state <= E1_STARTACK;
                end if;

            when others =>                                          --En cualquier otro caso
                nxt_state <= E0_NOMATCH;
                nxt_index <= CODE'low;
                nxt_index_puk <= CODE_PUK'low;
             
        end case;
    end process;
     
--next_decoder END--
    
 --output_decoder--
 --Salidas de los estados de la maquina.   
output_decoder: process (state)
    begin
        case state is
            when E0_NOMATCH =>
                DACK  <= '0';
                DONE  <= '1';
                MATCH <= '0';
                DACKrep  <= '0';
                DONErep  <= '1';
                MATCHrep <= '0';

            when E1_STARTACK =>
                DACK  <= '1';
                DONE  <= '0';
                MATCH <= '0';
                DACKrep  <= '1';
                DONErep  <= '0';
                MATCHrep <= '0';

            when E2_WTDIGIT =>
                DACK  <= '0';
                DONE  <= '0';
                MATCH <= '0';
                DACKrep  <= '0';
                DONErep  <= '0';
                MATCHrep <= '0';

            when E3_DIGACK =>
                DACK  <= '1';
                DONE  <= '0';
                MATCH <= '0';
                DACKrep  <= '1';
                DONErep  <= '0';
                if counter>3 then
                    MATCHrep <= '1';
                else MATCHrep <= '0';
                end if;

            when E4_MATCH =>
                DACK  <= '0';
                DONE  <= '1';
                MATCH <= '1';
                DACKrep  <= '0';
                DONErep  <= '1';
                MATCHrep <= '1';

            when others =>
                DACK  <= '0';
                DONE  <= '1';
                MATCH <= '0';
                DACKrep  <= '0';
                DONErep  <= '1';
                MATCHrep <= '0';
        end case;
    end process;
    --output_decoder END--
    --leds--
    -- En funcion del contador se asignaran valores a las se�ales para encender los leds correspondientes.
leds: process(counter)
begin
    case counter is                       --Shows the counter: por cada incremento del contador se encienden los leds
        when 0 => 
            LEDS_COUNTER<="0000";
        when 1 =>
            LEDS_COUNTER<="0001";
        when 2 => 
            LEDS_COUNTER<="0010";
        when 3 =>
            LEDS_COUNTER<="0011";
        when 4 =>
            LEDS_COUNTER<="0100";
        when others =>
            LEDS_COUNTER<="1111";
    end case;
end process;
--leds END--
end BEHAVIORAL;

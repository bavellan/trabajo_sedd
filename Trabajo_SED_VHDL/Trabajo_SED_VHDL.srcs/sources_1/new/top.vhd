library ieee;
use ieee.std_logic_1164.all;

use work.aux_types.all;

entity TOP is
    generic (
        CLKIN_FREQ : positive := 100_000_000;                         -- Nexys4 DDR clock frequency [Hz]
        CODE       : code_vector := ("0001", "0010", "0011", "0100");  -- Security code 
        CODE_PUK   : code_vector2 := ("1001", "1000", "0111", "0110")  -- Security code 
    );
    port (
        CLK100MHZ  : in    std_logic;                     -- Nexys4 DDR 100 MHz clock
        CPU_RESETN : in    std_logic;                     -- Nexys4 DDR reset button
        STM2FPGA         : in    std_logic_vector(5 downto 0);  -- Nexys4 DDR switches
        FPGA2STM        : out   std_logic_vector(2 downto 0);   -- Nexys4 DDR LEDs
        LEDREP        :out std_logic_vector(2 downto 0);
        LEDS_COUNTER       : out std_logic_vector(3 downto 0)
    );
end TOP;

architecture STRUCTURAL of TOP is
    constant SYS_CLK_FREQ : positive := 100;                          -- Frecuencia del reloj interno [Hz]

    -- Declaraci�n de componentes ---------------------------------------------
    component PRESCALER
        generic (
            CLKIN_FREQ  : positive;
            CLKOUT_FREQ : positive
        );
        port (
           RST_N  : in  std_logic;
           CLKIN  : in  std_logic;
           CLKOUT : out std_logic
        );
    end component;

    component SYNCHRONIZER is
        generic (
            WIDTH     : positive
        );
        port (
            CLK      : in  std_logic;
            ASYNC_IN : in  std_logic_vector (WIDTH - 1 downto 0);
            SYNC_OUT : out std_logic_vector (WIDTH - 1 downto 0)
        );
    end component;

    component DEBOUNCER
        generic (
            WIDTH   : positive
        );
        port (
            RST_N   : in  std_logic;
            CLK     : in  std_logic;
            ENTRADA : in  std_logic_vector(WIDTH - 1 downto 0);
            SALIDA  : out std_logic_vector(WIDTH - 1 downto 0)
        );
    end component;

    component MQUINA
        generic (
            CODE : code_vector;
            CODE_PUK:code_vector2
         );
        port(
            RST_N : in  std_logic;
            CLK   : in  std_logic;
            START : in  std_logic;  -- Request start password checking (from STM)
            DRDY  : in  std_logic;  -- Digit ready (from STM)
            DIGIT : in  std_logic_vector(3 downto 0);  -- Digit (from STM)
            DACK  : out std_logic;  -- Digit acknowledge (to STM)
            DONE  : out std_logic;  -- Password checking finished (to STM)
            MATCH : out std_logic;   -- Password match (to STM)
            LEDS_COUNTER   : out std_logic_vector(3 downto 0);
            DACKrep  : out std_logic;  -- LED SHIWU
            DONErep  : out std_logic;  -- 
            MATCHrep : out std_logic   -- 
     );                              
    end component;
    --------------------------------------------- Declaraci�n de componentes --

    -- Se�ales ----------------------------------------------------------------
    signal sys_clk : std_logic;                   -- 100 Hz system clock
    signal sync_sw : std_logic_vector(STM2FPGA'range);  -- Sync'ed async inputs
    signal filt_sw : std_logic_vector(STM2FPGA'range);  -- Debounced sync inputs
    ---------------------------------------------------------------- Se�ales --

    -- Aliases ----------------------------------------------------------------
    alias DIGIT : std_logic_vector(3 downto 0) is filt_sw(3 downto 0);
    alias DRDY  : std_logic is filt_sw(4);
    alias START : std_logic is filt_sw(5);
    alias DACK  : std_logic is FPGA2STM(0);
    alias DONE  : std_logic is FPGA2STM(1);
    alias MATCH : std_logic is FPGA2STM(2);
    alias DACKrep  : std_logic is LEDREP(0);
    alias DONErep  : std_logic is LEDREP(1);
    alias MATCHrep : std_logic is LEDREP(2);
    alias LEDC : std_logic_vector (3 downto 0) is LEDS_COUNTER(3 downto 0);
    ---------------------------------------------------------------- Aliases --
begin
    prescaler1: PRESCALER
        generic map (
            CLKIN_FREQ  => CLKIN_FREQ,
            CLKOUT_FREQ => SYS_CLK_FREQ
        )
        port map (
           RST_N  => CPU_RESETN,
           CLKIN  => CLK100MHZ,
           CLKOUT => sys_clk
        );

    synchro1: SYNCHRONIZER
        generic map (
            WIDTH => STM2FPGA'length
        )
        port map (
            CLK      => sys_clk,
            ASYNC_IN => STM2FPGA,
            SYNC_OUT => sync_sw
        );

    debouncer1: DEBOUNCER
        generic map (
            WIDTH => sync_sw'length
        )
        port map(
            RST_N   => CPU_RESETN,
            CLK     => sys_clk,
            ENTRADA => sync_sw,
            SALIDA  => filt_sw
        );

    mquina1: MQUINA
        generic map (
            CODE => CODE,
            CODE_PUK=>CODE_PUK
         )
        port map (
            RST_N => CPU_RESETN,
            CLK   => sys_clk,
            START => START,
            DRDY  => DRDY,
            DIGIT => DIGIT,
            LEDS_COUNTER=>LEDC,
            DACK  => DACK,
            DONE  => DONE,
            MATCH => MATCH,
            DACKrep  => DACKrep,
            DONErep  => DONErep,
            MATCHrep => MATCHrep
        );
end STRUCTURAL;

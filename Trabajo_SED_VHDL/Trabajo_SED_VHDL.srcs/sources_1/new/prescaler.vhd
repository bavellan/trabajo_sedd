library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PRESCALER is
    generic (
        CLKIN_FREQ  : positive;
        CLKOUT_FREQ : positive
    );
    port (
       RST_N  : in  std_logic;
       CLKIN  : in  std_logic;
       CLKOUT : out std_logic
    );
end PRESCALER;

architecture BEHAVIORAL of PRESCALER is
    signal clkout_i : std_logic;
begin
    gestion_CLK: process (CLKIN)
        constant TICKS_PER_SEMIPERIOD : positive := CLKIN_FREQ / (2 * CLKOUT_FREQ);
        subtype counter_t is integer range 0 to TICKS_PER_SEMIPERIOD - 1;
        variable counter : counter_t;
    begin
        if RST_N = '0' then
            counter := counter_t'high;
            clkout_i <= '0';
        elsif rising_edge(CLKIN) then
            if counter = 0 then
                counter := counter_t'high;
                clkout_i <= not clkout_i;
            else
                counter := counter - 1;
            end if;
        end if;
    end process;
    CLKOUT <= clkout_i;
end behavioral;
